#!/usr/bin/env bash
# wait-for-postgres.sh

set -o errexit
set -o pipefail
set -o nounset


# Check that $DATABASE_URL is set, fail otherwise:
if [[ -z "${DATABASE_URL:-}" ]]; then
  echo 'Error: DATABASE_URL is not set.'
  echo 'Application will not start.'
  exit 1
fi

postgres_ready() {
python << END
import sys

import psycopg2
import environ

env = environ.Env()
default_db = env.db('DATABASE_URL')

try:
    psycopg2.connect(
        dbname=default_db['NAME'],
        user=default_db['USER'],
        password=default_db['PASSWORD'],
        host=default_db['HOST'],
        port=default_db['PORT'],
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}

until postgres_ready; do
  >&2 echo 'Waiting for PostgreSQL to become available...'
  sleep 1
done
>&2 echo 'Postgres is up - executing command'

exec "$@"
