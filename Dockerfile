FROM python:3.8-slim-buster

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIPENV_HIDE_EMOJIS=true \
    PIPENV_NOSPIN=true

# System deps
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libgettextpo-dev \
        mime-support \
		nano \
        gcc \
        libpq-dev \
	&& rm -rf /var/lib/apt/lists/*

# Copy the watchman executable binary from icalialabs image.
COPY --from=icalialabs/watchman:buster /usr/local/bin/watchman* /usr/local/bin/
RUN mkdir -p /usr/local/var/run/watchman

WORKDIR /usr/src/app

# Install app dependencies
COPY requirements.txt ./
RUN pip install -r requirements.txt

ADD . .

EXPOSE 8000

COPY ./wait-for-postgres.sh /wait-for-postgres.sh
COPY ./docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
