import os
import boto3
import environ

from builtins import bool, str



# ======================= BASE ===============================================
from django.utils import timezone

BASE_DIR = environ.Path(__file__) - 2

# ======================= ENV ================================================
env = environ.Env(
    DEBUG=(bool, True),
    APP_INSTANCE=(str, 'prod'),
)
environ.Env.read_env(env_file=os.path.join(BASE_DIR, '.env'))

DEBUG = env.bool('DEBUG', False)
APP_INSTANCE = env('APP_INSTANCE')

# ======================= SECURITY ===========================================
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=[])
ADMIN_URL = env('ADMIN_URL', default='admin/')
SECRET_KEY = env('SECRET_KEY', default='y$o97r*90ixip94186+%_7&2o018hlyvd8n3hzecf6gyirnd4r')
TEST_ENV = env.bool('TEST_ENV', default=False)
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
ADMIN_EMAILS = env.list('ADMIN_EMAILS', default=[])
USE_AWS = env.bool('USE_AWS', False)
AWS_ACCESS_KEY_ID = env.str('AWS_ACCESS_KEY_ID', default='')
AWS_SECRET_ACCESS_KEY = env.str('AWS_SECRET_ACCESS_KEY', default='')
# ======================= APPLICATIONS =======================================
DJANGO_APPS = [
    'jazzmin',
    'applications.app.CustomAdminConfig',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.postgres',
    'django.contrib.sessions',
    'django.contrib.messages',
    'collectfast',
    'django.contrib.staticfiles',
]
CUSTOM_APPS = [
    'applications.users',
    'applications.core',
    'applications.treks',
    'applications.orders',
    'applications.news',
]
THIRD_PART_APPS = [
    'corsheaders',
    'django_extensions',
    'rest_framework',
    'drf_yasg',
    'rest_framework_filters',
    'ckeditor',
    'ckeditor_uploader',
]

INSTALLED_APPS = DJANGO_APPS + CUSTOM_APPS + THIRD_PART_APPS

# ======================= MIDDLEWARE =========================================
MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'applications.core.middlewares.TimezoneMiddleware',
    'applications.core.middlewares.TranslationMiddleware',
]

AUTH_USER_MODEL = 'users.User'

# ======================= URLS ===============================================
ROOT_URLCONF = 'config.urls'

# ======================= TEMPLATES ==========================================
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
            ],
        },
    },
]

# REST
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ['rest_framework.permissions.IsAuthenticated'],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'applications.core.authentication.FirebaseAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_FILTER_BACKENDS': [
        'rest_framework_filters.backends.RestFrameworkFilterBackend',
    ],
    'EXCEPTION_HANDLER': 'config.handlers.custom_exception_handler',
    'COERCE_DECIMAL_TO_STRING': False,
}

if TEST_ENV:
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = ['rest_framework.authentication.SessionAuthentication']

# ======================= USGI ===============================================
WSGI_APPLICATION = 'config.wsgi.application'

# ======================= DATABASES ==========================================
DATABASES = {
    'default': env.db('DATABASE_URL'),
}

# ======================= CACHE ==========================================
COLLECTFAST_ENABLED = False
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': env.str('CACHE_URL', ''),
        'OPTIONS': {
            "CLIENT_CLASS": 'django_redis.client.DefaultClient'
        },
    }
}
if TEST_ENV:
    CACHES['default'] = {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }

# ======================= LOCALE =============================================
LANGUAGE_CODE = 'ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# ======================= STATIC =============================================
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'staticfiles/'),
)
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

# https://django-ckeditor.readthedocs.io/en/latest/#optional-customizing-ckeditor-editor
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Additional',
        'toolbar_Additional': [
            ['Styles', 'Format', 'Bold', 'Italic', 'Underline', 'Strike', 'Undo', 'Redo'],
            ['Link', 'Unlink', 'Anchor'],
            ['Image', 'Flash', 'Table', 'HorizontalRule'],
            ['TextColor', 'BGColor'],
            ['SpecialChar'],
            ['Source'],
            ['EmojiLink']
        ],
        'removePlugins': ['smiley'],
    },
}
CKEDITOR_UPLOAD_PATH = 'ckeditor_uploads/'

# ======================= MEDIA =============================================
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# ======================= LOGGING ================================================
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'INFO',
        'handlers': ['console'],
    },
    'formatters': {
        'json': {'()': 'pythonjsonlogger.jsonlogger.JsonFormatter'},
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'json',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.security': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
        'gunicorn': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': True,
        },
    }
}

if USE_AWS:
    client = boto3.client(
        'logs',
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
        region_name=env.str('AWS_REGION_NAME', default=''),
    )
    LOGGING['root']['handlers'].append('watchtower')
    LOGGING['handlers']['watchtower'] = {
        'level': 'INFO',
        'class': 'watchtower.CloudWatchLogHandler',
        'boto3_client': client,
        'log_group_name': env.str('AWS_LOG_GROUP', default=''),
        'stream_name': 'racek-server',
        'formatter': 'json',
    }
# ==========

# ======== Admin interface ========
JAZZMIN_SETTINGS = {
    "site_icon": None,
    "welcome_sign": "Welcome to the racek",
    "copyright": "Racek Ltd",

    "topmenu_links": [
        {"name": "Home", "url": "admin:index", "permissions": ["auth.view_user"]},
    ],
    # Whether to display the side menu
    "show_sidebar": True,
    # Whether to aut expand the menu
    "navigation_expanded": True,

    # for the full list of 5.13.0 free icon classes
    "icons": {
        "auth": "fas fa-users-cog",
        "auth.user": "fas fa-user",
        "auth.Group": "fas fa-users",
    },
    # Icons that are used when one is not manually specified
    "default_icon_parents": "fas fa-chevron-circle-right",
    "default_icon_children": "fas fa-circle",
    "related_modal_active": False,
    "custom_css": None,
    "custom_js": None,
    "show_ui_builder": False,
    "changeform_format": "horizontal_tabs",
}
JAZZMIN_UI_TWEAKS = {
    "theme": "litera",
    "dark_mode_theme": "darkly",
}

FIREBASE_CREDENTIALS = {
    'type': 'service_account',
    'project_id': env.str('FIREBASE_PROJECT_ID', default=''),
    'private_key_id': env.str('FIREBASE_PRIVATE_KEY_ID', default=''),
    'private_key': env.str('FIREBASE_PRIVATE_KEY', default='', multiline=True),
    'client_email': env.str('FIREBASE_CLIENT_EMAIL', default=''),
    'client_id': env.str('FIREBASE_CLIENT_ID', default=''),
    'auth_uri': 'https://accounts.google.com/o/oauth2/auth',
    'token_uri': 'https://oauth2.googleapis.com/token',
    'auth_provider_x509_cert_url': 'https://www.googleapis.com/oauth2/v1/certs',
    'client_x509_cert_url': env.str('FIREBASE_CLIENT_X509_CERT_URL', default='')
}

CLOUD_FRONT_ENABLED = env.bool('CLOUD_FRONT_ENABLED', False)
if USE_AWS:
    AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME')
    AWS_DEFAULT_ACL = 'public-read'
    AWS_S3_CUSTOM_DOMAIN = f'{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com'
    if CLOUD_FRONT_ENABLED:
        AWS_S3_CUSTOM_DOMAIN = env.str('AWS_CLOUD_FRONT_DOMAIN')
    AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}
    AWS_LOCATION = 'static'
    STATIC_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_LOCATION}/'
    STATICFILES_STORAGE = 'config.storages.StaticStorage'
    PUBLIC_MEDIA_LOCATION = 'media'
    MEDIA_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{PUBLIC_MEDIA_LOCATION}/'
    DEFAULT_FILE_STORAGE = 'config.storages.PublicMediaStorage'
    COLLECTFAST_ENABLED = True
    COLLECTFAST_STRATEGY = 'collectfast.strategies.boto3.Boto3Strategy'
