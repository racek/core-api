from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from applications.api.urls import customer_patterns, guide_patterns, tour_company_patterns
from applications.core.views import get_private_policy, main_page_view

admin.autodiscover()
admin.site.enable_nav_sidebar = False

schema_view = get_schema_view(
    openapi.Info(
        title='Core API',
        default_version='v1',
        contact=openapi.Contact(email='ersul4ik@gmail.com'),
    ),
    public=True,
    permission_classes=(permissions.IsAdminUser,),
)

api_patterns = [
    path('customer/', include(customer_patterns)),
    path('guide/', include(guide_patterns)),
    path('companies/', include(tour_company_patterns)),
]

urlpatterns = [
    path('', main_page_view),
    path(settings.ADMIN_URL, admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/v1/', include(api_patterns)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('confidentiality/', get_private_policy, name='schema-swagger-ui'),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
