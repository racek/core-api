# Core API

Welcome to Racek, a web application designed for tour companies to manage hiking tours and for customers to find and book tours.
Our application provides an API that powers our own mobile application and an admin dashboard for tour companies to manage their tours and customers.
With Racek, we aim to make it easier for tour companies to manage their business and for customers to find and book their dream hiking tours.




The service provides:

  - API. CRUD for entities (http://{hostname}/docs/)
  - Dashboard:     
    - creating staff managers.
    - create tours.
    - read existing entities.

### Tech

* [Python3](https://phoenixnap.com/kb/how-to-install-python-3-ubuntu) - The main language
* [Django3](https://tecadmin.net/install-django-on-ubuntu/) - Provides API/Admin dashboard
* [Pytest](https://docs.pytest.org/en/stable/) - Testing.
* [Docker](https://www.docker.com/) - Environment

#### Third party apps

* [jazzmin](https://django-jazzmin.readthedocs.io/) - Django custom admin dashboard
* [Django CKEditor](https://django-ckeditor.readthedocs.io/en/latest/) - Django admin CKEditor integration..
* [Collectfast](https://pypi.org/project/Collectfast/) - Efficiently decide what files to upload using cached checksums
* [etc](https://gitlab.com/racek/core-api/-/blob/master/config/settings.py#L35)

### How I can run the app

- `sudo apt install libsqlite3-dev` - if you do not have it
- `pyenv install 3.8` - note, that you need specity the exact version of Python3.8, pyenv will show you
- `pipenv --python ~/.pyenv/versions/3.8.x/bin/python install` - for installing python environment
- `pipenv shell` - to activate virtual environment
- `pipenv install` - to install needed package from Pipfile
- `./manage.py migrate` - apply database migration with minimal changes
- `./manage.py runserver 8000` - for running server

### How I can run the app with docker

- You must install docker
- You must install docker-compose
- Run the command `docker-compose up` (on level with docker-compose.yml)

### Tests
- `pytest`. Use this command to make sure that everything is ok.


### Project structure

```
├── applications      business logic layer  
├── config            project configuration
├── docs              documenation files 
├── nginx             configuration files for the Nginx web server used to proxy requests to the application           
├── static            css files for local development         
├── staticfiles       *static files for production development (debug=false)
└── templates         html files
└── tests             tests in order to check *applications
└── README            
```


### Troubleshooting

- If you have a problem with static files (locally):
  - set DEBUG=False in settings.py
    
### Todos

 - https://www.notion.so/d85bc357ec0f41bb93a8bcebc1893cf5?v=636e51a53edd4e1da3dc232e58943004

### Team

 - Telegram: @ersul4ik
 - Telegram: @e_akmatov
