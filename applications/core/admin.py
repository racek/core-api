from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.db.models import TextField

from applications.core.forms import PrivatePolicyAdminForm
from applications.core.models import PrivatePolicy, AreaInformation
from applications.utils.admin import AdminPermissionRequireMixin


@admin.register(PrivatePolicy)
class PrivatePolicyAdmin(admin.ModelAdmin):
    form = PrivatePolicyAdminForm

    def has_add_permission(self, request):
        return all([request.user.is_administrator, not PrivatePolicy.objects.exists()])

    def has_view_permission(self, request, obj=None):
        return request.user.is_administrator


@admin.register(AreaInformation)
class AreaInformationAdmin(AdminPermissionRequireMixin, admin.ModelAdmin):
    formfield_overrides = {
        TextField: {'widget': CKEditorUploadingWidget}
    }
