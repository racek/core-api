from django.db import models
from django_extensions.db.models import TimeStampedModel


class PrivatePolicy(TimeStampedModel):
    is_active = models.BooleanField(default=True, unique=True)
    content = models.TextField()

    class Meta:
        verbose_name_plural = 'Private policies'

    def __str__(self):
        return self.content[:50]


class AreaInformation(models.Model):
    title = models.CharField(max_length=125)
    text = models.TextField()

    class Meta:
        verbose_name = 'Information about area'
        verbose_name_plural = 'Information about area'

    def __str__(self) -> str:
        return self.title
