from django.core.exceptions import ImproperlyConfigured
from django.utils import timezone, translation
from django.utils.deprecation import MiddlewareMixin


class TimezoneMiddleware(MiddlewareMixin):
    """
    Enable timezone specified at user profile for staff members.
    https://docs.djangoproject.com/en/2.1/topics/i18n/timezones/#selecting-the-current-time-zone
    """

    def process_request(self, request):
        # AuthenticationMiddleware is required so that request.user exists.
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The Django remote user auth middleware requires the"
                " authentication middleware to be installed.  Edit your"
                " MIDDLEWARE setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the TimezoneMiddleware class.")
        user = request.user
        if user.is_authenticated and user.is_staff:
            timezone.activate(user.tz)

    def process_response(self, request, response):
        timezone.deactivate()
        return response


class TranslationMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The Django remote user auth middleware requires the"
                " authentication middleware to be installed.  Edit your"
                " MIDDLEWARE setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the TimezoneMiddleware class.")
        if request.user.is_authenticated and request.user.is_customer:
            translation.activate(request.user.language)

    def process_response(self, request, response):
        translation.deactivate()
        return response
