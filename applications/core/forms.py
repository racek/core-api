from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms

from applications.core.models import PrivatePolicy


class PrivatePolicyAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = PrivatePolicy
        fields = '__all__'
