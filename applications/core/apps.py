from django.apps import AppConfig


class CoreConfig(AppConfig):
    """ Configuration for core application """
    name = 'core'
