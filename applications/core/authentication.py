import firebase_admin
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.encoding import smart_text
from django.utils.translation import gettext as _
from firebase_admin import auth, credentials
from rest_framework import authentication, exceptions

from applications.utils.formats import split_display_name

if settings.FIREBASE_CREDENTIALS and not settings.TEST_ENV:
    creds = credentials.Certificate(settings.FIREBASE_CREDENTIALS)
else:
    creds = None
firebase_admin.initialize_app(creds)

FIREBASE_AUTH_HEADER_PREFIX = 'JWT'


class FirebaseAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using Firebase authentication.  Otherwise returns `None`.
        """
        firebase_token = self.get_token(request)
        if firebase_token is None:
            return None

        try:
            payload = auth.verify_id_token(firebase_token)
        except auth.RevokedIdTokenError:
            raise exceptions.AuthenticationFailed('ID token has been revoked.')
        except auth.ExpiredIdTokenError:
            raise exceptions.AuthenticationFailed('ID token is expired.')
        except auth.InvalidIdTokenError:
            raise exceptions.AuthenticationFailed('ID token is invalid.')

        lang = request.META.get('HTTP_ACCEPT_LANGUAGE', settings.LANGUAGE_CODE)
        user = self._authenticate_credentials(payload, lang)
        return user, payload

    # noinspection PyMethodMayBeStatic
    def _authenticate_credentials(self, payload, lang):
        """
        Returns an active user that matches the payload's user id.
        """
        User = get_user_model()
        uid = payload['uid']
        if not uid:
            raise exceptions.AuthenticationFailed('Invalid payload.')

        try:
            user = User.objects.get(username=uid, is_active=True)
        except User.DoesNotExist:
            firebase_user = auth.get_user(uid)
            photo_url = firebase_user.photo_url if firebase_user.photo_url else ""
            phone_number = firebase_user.phone_number if firebase_user.phone_number else ""

            # Attempt to fill user with firebase display name
            if firebase_user.display_name:
                names = split_display_name(firebase_user.display_name)
                first_name, last_name = names[0], names[1]
            else:
                first_name, last_name = '', ''

            defaults = {
                'fb_photo_url': photo_url,
                'phone_number': phone_number,
                'first_name': first_name,
                'last_name': last_name,
                'language': lang,
                'is_customer': True,
            }
            user, _ = User.objects.get_or_create(username=firebase_user.uid, defaults=defaults)
            return user

        if not user.is_active:
            raise exceptions.AuthenticationFailed('User account is disabled')

        return user

    # noinspection PyMethodMayBeStatic
    def get_token(self, request):
        _auth = authentication.get_authorization_header(request).split()
        auth_header_prefix = FIREBASE_AUTH_HEADER_PREFIX.lower()

        if not _auth:
            return None

        if len(_auth) == 1:
            msg = _('Invalid Authorization header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(_auth) > 2:
            msg = _('Invalid Authorization header. Credentials string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        if smart_text(_auth[0].lower()) != auth_header_prefix:
            return None

        return _auth[1]
