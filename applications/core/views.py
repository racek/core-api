from django.http import Http404
from django.shortcuts import render

from applications.core.models import PrivatePolicy


def main_page_view(request):
    return render(request, 'home.html')


def get_private_policy(request):
    policy = PrivatePolicy.objects.first()
    if not policy:
        raise Http404('Policy does not exist')
    return render(request, 'private_policy.html', {'policy': policy})
