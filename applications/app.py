from django.contrib import admin
from django.contrib.admin.apps import AdminConfig
from django.urls import path

from applications.treks.views import TripsBasedOnPastView


class CustomAdminSite(admin.AdminSite):
    """ Customization for admin site """
    site_header = 'Racek management'
    index_title = 'Racek'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('tours-based-on-template/', TripsBasedOnPastView.as_view(), name='tours_based_on_template'),
        ]
        return urls + custom_urls


class CustomAdminConfig(AdminConfig):
    """ Customization of custom admin config """
    default_site = 'applications.app.CustomAdminSite'
