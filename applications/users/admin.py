from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from django.contrib.auth.models import Group
from django.utils.html import format_html

from applications.users.forms import StaffAdminForm
from applications.users.models import Guide, Customer, Staff, TourGroup, User
from applications.utils.admin import ReadOnlyPermissionAdminMixin


@admin.register(User)
class UserAdmin(ReadOnlyPermissionAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'fullname',
        'user_type',
    )


@admin.register(Staff)
class StaffAdmin(DefaultUserAdmin):
    form = StaffAdminForm
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal information', {'fields': (
            'first_name',
            'last_name',
            'email',
            'company',
        )}),
        ('Permissions', {'fields': (
            'is_active',
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions',
        )}),
    )


@admin.register(Customer)
class CustomerAdmin(ReadOnlyPermissionAdminMixin, admin.ModelAdmin):
    fields = (
        'first_name',
        'last_name',
        'phone_number',
        'city',
        'get_photo_url',
    )
    readonly_fields = (
        'get_photo_url',
    )

    def get_photo_url(self, obj):
        if obj.fb_photo_url:
            return format_html('<img src="{}" height="50" width="50" />', obj.fb_photo_url)
        return obj.fb_photo_url

    get_photo_url.short_description = 'Photo'


@admin.register(Guide)
class GuideAdmin(DefaultUserAdmin):
    form = StaffAdminForm
    fieldsets = (
        ('Personal information', {'fields': (
            'username',
            'first_name',
            'last_name',
            'phone_number',
            'guide_avatar',
            'company',
        )}),
        ('Permissions', {'fields': (
            'is_active',
            'is_guide',
            'is_staff',
            'is_superuser',
        )}),
    )


@admin.register(TourGroup)
class TourGroupAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return request.user.is_administrator

    def has_delete_permission(self, request, obj=None):
        return request.user.is_administrator

    def has_change_permission(self, request, obj=None):
        return True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if not request.user.is_administrator:
            return qs.filter(id=request.user.company_id)
        return qs


admin.site.unregister(Group)
