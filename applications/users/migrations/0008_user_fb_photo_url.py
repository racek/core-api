# Generated by Django 3.1.5 on 2021-04-12 06:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20210218_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='fb_photo_url',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='photo'),
        ),
    ]
