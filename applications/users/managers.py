from django.contrib.auth.models import UserManager


class StaffManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_staff=True, is_customer=False, is_guide=False)


class CustomerManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_customer=True, is_guide=False, is_staff=False, is_superuser=False)


class GuideManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_guide=True)
