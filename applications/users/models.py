import uuid

import pytz
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from applications.users.managers import CustomerManager, GuideManager, StaffManager


class TourGroup(models.Model):
    title = models.CharField(max_length=255)
    icon = models.ImageField('icon link', upload_to='companies-icons/')
    description = models.TextField('description', default='')
    email = models.EmailField('company email', default='')
    code = models.UUIDField(default=uuid.uuid4)

    def __str__(self):
        return self.title


class User(AbstractUser):
    BISHKEK = 'bishkek'
    OSH = 'osh'
    BALYKCHY = 'balykchy'
    CITIES = (
        (BISHKEK, 'Bishkek'),
        (OSH, 'Osh'),
        (BALYKCHY, 'Balykchy'),
    )
    ENGLISH = 'en'
    RUSSIAN = 'ru'
    LANGUAGES = (
        (ENGLISH, 'English'),
        (RUSSIAN, 'Russian'),
    )
    is_customer = models.BooleanField(default=False)
    is_guide = models.BooleanField(default=False)
    company = models.ForeignKey(TourGroup, on_delete=models.PROTECT, blank=True, null=True)
    phone_number = PhoneNumberField(blank=True)
    city = models.CharField(
        'city', max_length=12, choices=CITIES,
        default=BISHKEK, help_text='The city where the customer is from',
    )
    language = models.CharField(max_length=10, choices=LANGUAGES, default=RUSSIAN)
    fb_photo_url = models.CharField('photo', max_length=255, blank=True, default='')
    guide_avatar = models.ImageField(upload_to='guide-avatars/', blank=True, null=True)

    def __str__(self):
        return f'{self.pk} - {self.fullname}'

    @property
    def fullname(self):
        return self.get_full_name() if self.get_full_name() else self.username

    @property
    def tz(self):
        if self.city == self.BISHKEK:
            return pytz.timezone('Asia/Bishkek')
        return pytz.timezone('UTC')

    @property
    def is_administrator(self):
        return self.email in settings.ADMIN_EMAILS

    @property
    def user_type(self):
        if self.is_administrator:
            return 'Administrator'
        elif self.is_customer:
            return 'Customer'
        elif self.is_guide:
            return 'Guide'
        return 'Staff'


class Staff(User):
    objects = StaffManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.is_staff:
            self.is_staff = True
        super().save(*args, **kwargs)


class Customer(User):
    objects = CustomerManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.is_customer:
            self.is_customer = True
        super().save(*args, **kwargs)


class Guide(User):
    objects = GuideManager()

    def __str__(self):
        return self.username

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.is_guide:
            self.is_guide = True
        super().save(*args, **kwargs)
