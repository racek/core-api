from django import forms


class StaffAdminForm(forms.ModelForm):
    def clean(self):
        company = self.cleaned_data.get('company')
        if company is None and not self.cleaned_data.get('is_superuser'):
            raise forms.ValidationError({'company': 'Company is required'})
