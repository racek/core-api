import random

from django.db import models
from django.utils import timezone
from django_extensions.db.models import TimeStampedModel
from phonenumber_field.modelfields import PhoneNumberField


class OrderQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status__in=[Order.IDLE, Order.CONFIRMED], tour__arrival_at__gte=timezone.now())


class Order(TimeStampedModel):
    IDLE = 1
    CONFIRMED = 2
    CANCELED = 3
    USED = 4
    MISSED = 5
    STATUSES = (
        (IDLE, 'Idle'),
        (CONFIRMED, 'Confirmed'),
        (CANCELED, 'Canceled'),
        (USED, 'Used'),
        (MISSED, 'Missed'),
    )
    buyer = models.ForeignKey('users.Customer', on_delete=models.PROTECT)
    tour = models.ForeignKey('treks.Tour', on_delete=models.PROTECT)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=IDLE)
    price = models.DecimalField(max_digits=17, decimal_places=2)
    note = models.TextField(help_text="Buyer's note", default='')
    seats = models.PositiveSmallIntegerField(help_text='How many seats the user has booked.', default=1)
    code = models.PositiveSmallIntegerField(blank=True, null=True)

    objects = OrderQuerySet.as_manager()

    def __str__(self):
        return f'{self.buyer} - {self.tour}'

    @classmethod
    def generate_code(cls, tour_id):
        codes = cls.objects.filter(tour=tour_id).values_list('code', flat=True)
        code = random.randint(1, 500)
        while code in list(codes):
            code = random.randint(1, 500)
        return code

    @property
    def is_cancelable(self):
        return (self.tour.departure_at - timezone.now()) > timezone.timedelta(minutes=self.tour.cancel_deadline)


class AdditionalPassenger(models.Model):
    order = models.ForeignKey('orders.Order', on_delete=models.CASCADE, related_name='additional_passengers')
    fullname = models.CharField('full name', max_length=255)
    phone_number = PhoneNumberField(blank=True)

    def __str__(self):
        return self.fullname


class OrderFeedback(TimeStampedModel):
    order = models.ForeignKey('orders.Order', on_delete=models.CASCADE, related_name='feedbacks')
    customer = models.ForeignKey('users.Customer', on_delete=models.CASCADE, related_name='reviews')
    guide = models.ForeignKey('users.Guide', on_delete=models.CASCADE, related_name='feedbacks')
    tour_company = models.ForeignKey(
        'users.TourGroup', on_delete=models.CASCADE, related_name='feedbacks', null=True,
    )
    tour_score = models.PositiveSmallIntegerField()
    guide_score = models.PositiveSmallIntegerField()
    message = models.TextField(default='')
    reaction_message = models.TextField(default='')

    def __str__(self):
        return f'{self.order}: {self.tour_score}'
