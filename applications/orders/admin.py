from django.contrib import admin

from applications.orders.models import Order, AdditionalPassenger
from applications.utils.admin import ReadOnlyPermissionAdminMixin


class AdditionalPassengerInline(admin.TabularInline):
    model = AdditionalPassenger
    extra = 3


@admin.register(Order)
class OrderAdmin(ReadOnlyPermissionAdminMixin, admin.ModelAdmin):
    list_display = (
        '__str__',
        'status',
        'created',
    )
    inlines = [AdditionalPassengerInline]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if not request.user.is_administrator:
            return qs.filter(tour__owner=request.user.company_id)
        return qs
