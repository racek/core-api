from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Sum
from django.utils import timezone
from django_extensions.db.models import TimeStampedModel


class Location(models.Model):
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.city} ({self.country})'


class TourImage(models.Model):
    image = models.ImageField(upload_to='tour-images/')
    tour = models.ForeignKey('treks.Tour', on_delete=models.CASCADE, related_name='images')


class Tour(TimeStampedModel):
    IDLE = 1
    STARTED = 2
    FINISHED = 3
    CURRENCY_KGS = 'KGS'
    CURRENCY_CODES = (
        (CURRENCY_KGS, 'KGS'),
    )
    EASY = 'easy'
    MEDIUM = 'medium'
    HARD = 'hard'
    COMPLEXITIES = (
        ('easy', 'Easy'),
        ('medium', 'Medium'),
        ('hard', 'Hard'),
    )
    STATUSES = (
        (IDLE, 'Idle'),
        (STARTED, 'Started'),
        (FINISHED, 'Finished'),
    )
    location = models.ForeignKey('treks.Location', on_delete=models.PROTECT, null=True)
    owner = models.ForeignKey('users.TourGroup', related_name='tours', on_delete=models.PROTECT)
    guide = models.ForeignKey('users.Guide', blank=True, null=True, related_name='tours', on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(max_digits=17, decimal_places=2)
    currency = models.CharField(max_length=3, choices=CURRENCY_CODES, default=CURRENCY_KGS)
    duration = models.PositiveSmallIntegerField('duration in days')
    height = models.PositiveSmallIntegerField('height feet', help_text='Height feet in meters', default=0)
    distance = models.PositiveSmallIntegerField('distances in meters', help_text='Route distance', default=0)
    pickup_time = models.TimeField(
        'Pick-Up Time', help_text='The time when people are to meet on the place.', null=True,
    )
    pickup_address = models.CharField('Pick-Up Place', max_length=255, default='')
    departure_at = models.DateTimeField('departure time', help_text='The time when the tour will be start.')
    arrival_at = models.DateTimeField(blank=True, null=True, help_text='When the tour is expected to be over.')
    tags = models.ManyToManyField('treks.TourTag', related_name='tours')
    categories = models.ManyToManyField('treks.TourCategory')
    active = models.BooleanField(default=False)
    cancel_deadline = models.PositiveSmallIntegerField(help_text='Deadline to cancel order (in hours).', default=1)
    actual_finished_at = models.DateTimeField(
        blank=True, null=True, help_text='The actual time when the tour was finished.'
    )
    available_tickets_count = models.PositiveSmallIntegerField(
        default=1, help_text='How many customers can order this tour.'
    )
    finisher = models.ForeignKey(
        'users.User', on_delete=models.PROTECT,
        blank=True, null=True, help_text='The person who completed the tour.',
    )
    complexity = models.CharField(choices=COMPLEXITIES, max_length=10, default=EASY)
    sights = models.ManyToManyField('treks.TourSight', related_name='tours')
    opportunities = models.ManyToManyField('treks.TourOpportunity', related_name='tours')
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=IDLE)
    max_seats_per_booking = models.PositiveSmallIntegerField(
        'max seats per booking', default=3,
        validators=[MinValueValidator(1), MaxValueValidator(50)],
        help_text='Max number of allowed seats per customer booking.',
    )

    def __str__(self):
        return f'{self.title}'

    def save(self, **kwargs):
        if not self.arrival_at:
            self.arrival_at = self.departure_at + timezone.timedelta(days=self.duration)
        super().save(**kwargs)

    def get_available_for_purchase_count(self):
        booked_tickets = self.order_set.active().aggregate(Sum('seats'))['seats__sum']
        if not booked_tickets:
            booked_tickets = 0
        return self.available_tickets_count - booked_tickets


class TourTag(models.Model):
    """The tour category."""
    name = models.CharField(max_length=125)

    class Meta:
        unique_together = ['name']
        verbose_name_plural = 'Tags'

    def __str__(self):
        return self.name


class CustomerFavoriteTour(TimeStampedModel):
    customer = models.ForeignKey('users.Customer', on_delete=models.CASCADE, related_name='favorite_tours')
    tour = models.ForeignKey('treks.Tour', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Favorite customer tour'
        verbose_name_plural = 'Favorite customer tours'

    def __str__(self):
        return f'{self.tour} {self.created}'


class TourCategory(models.Model):
    """running, hiking, fishing."""
    title = models.CharField(max_length=125, unique=True)

    class Meta:
        verbose_name_plural = 'Tour categories'

    def __str__(self):
        return self.title


class TourSight(models.Model):
    """waterfall, lake, river."""
    title = models.CharField(max_length=125, unique=True)

    def __str__(self):
        return self.title


class TourOpportunity(models.Model):
    """dog_friendly, kid_friendly."""
    title = models.CharField(max_length=125, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Tour opportunities'
