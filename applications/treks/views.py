from django.contrib import messages
from django.contrib.admin import AdminSite
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from applications.treks.forms import TourBasedOnTemplateForm


class TripsBasedOnPastView(View):
    template_name = 'admin/treks/tour_based_on_template_change.html'
    form_class = TourBasedOnTemplateForm
    site = AdminSite()
    context = {
        'title': 'Create tour based on the template',
        'site_header': site.site_header,
        'site_url': site.site_url,
    }

    def get(self, request, *args, **kwargs):
        self.context['user'] = request.user
        self.context['has_permission'] = self.site.has_permission(request)
        self.context['form'] = self.form_class()
        return render(request, self.template_name, self.context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if not form.is_valid():
            self.context['form'] = form
            return render(request, self.template_name, self.context)

        messages.success(request, 'Trips will be created soon')
        return HttpResponseRedirect(reverse('admin:treks_tour_changelist'))
