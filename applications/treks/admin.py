from django.contrib import admin, messages
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe

from applications.orders.models import Order
from applications.treks.forms import TourAdminForm
from applications.treks.models import (
    Tour,
    TourImage,
    TourTag,
    CustomerFavoriteTour,
    TourSight,
    TourOpportunity,
    TourCategory,
    Location,
)
from applications.utils.admin import AdminPermissionRequireMixin, ReadOnlyPermissionAdminMixin


class TourImageInline(admin.TabularInline):
    model = TourImage
    extra = 3


@admin.register(Tour)
class TourAdmin(admin.ModelAdmin):
    form = TourAdminForm
    change_list_template = 'admin/treks/tours_changelist.html'
    list_display = (
        '__str__',
        'departure_at',
        'arrival_at',
        'actual_finished_at',
        'active',
    )
    inlines = [TourImageInline]
    actions = ['make_finished']
    readonly_fields = (
        'finisher',
        'actual_finished_at',
        'status',
    )

    def save_model(self, request, obj, form, change):
        if not request.user.is_administrator and request.user.company:
            obj.owner = request.user.company
        return super().save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_administrator:
            self.exclude = ('owner',)
        if not request.user.company:
            url = reverse('admin:users_staff_change', args=(request.user.id, ))
            msg = mark_safe(f"Current user has not selected company.  <a href='{url}'>Select the company</a>")
            self.message_user(request, msg, level=messages.ERROR)
        return super().get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if not request.user.is_administrator:
            return qs.filter(owner=request.user.company_id)
        return qs

    def make_finished(self, request, queryset):
        queryset.update(finisher=request.user, actual_finished_at=timezone.now())
        for tour in queryset:
            tour.order_set.filter(status=Order.CONFIRMED).update(status=Order.USED)
            tour.order_set.filter(status=Order.IDLE).update(status=Order.MISSED)

    make_finished.short_description = "Mark selected tours as finished"


@admin.register(TourCategory)
class TourCategoryAdmin(AdminPermissionRequireMixin, admin.ModelAdmin):
    pass


@admin.register(TourTag)
class TourTagAdmin(AdminPermissionRequireMixin, admin.ModelAdmin):
    pass


@admin.register(CustomerFavoriteTour)
class CustomerFavoriteTourAdmin(ReadOnlyPermissionAdminMixin, admin.ModelAdmin):
    pass


@admin.register(TourSight)
class TourSightsAdmin(AdminPermissionRequireMixin, admin.ModelAdmin):
    pass


@admin.register(TourOpportunity)
class OpportunityAdmin(AdminPermissionRequireMixin, admin.ModelAdmin):
    pass


@admin.register(Location)
class LocationAdmin(AdminPermissionRequireMixin, admin.ModelAdmin):
    pass
