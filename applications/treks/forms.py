from django import forms
from django.contrib.admin import widgets


class TourBasedOnTemplateForm(forms.Form):
    pickup_time = forms.TimeField(widget=widgets.AdminTimeWidget)
    departure_at = forms.DateTimeField(widget=widgets.AdminSplitDateTime)
    arrival_at = forms.DateTimeField(widget=widgets.AdminSplitDateTime)
    pickup_address = forms.CharField(widget=widgets.AdminTextInputWidget)
    price = forms.DecimalField(
        widget=widgets.AdminIntegerFieldWidget,
        max_digits=17, decimal_places=2, initial=0, min_value=0,
    )


class TourAdminForm(forms.ModelForm):
    def clean(self):
        owner = self.cleaned_data.get('owner')
        if not owner:
            raise forms.ValidationError("The tour owner is not selected.")
        return self.cleaned_data

