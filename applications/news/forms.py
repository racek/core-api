from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms

from applications.news.models import CustomerNews


class CustomerNewsForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorUploadingWidget(), required=False)

    def clean(self):
        if not self.cleaned_data.get('customers') and not self.cleaned_data.get('public'):
            raise forms.ValidationError('News can be public or private. Please select either of that.')

    class Meta:
        model = CustomerNews
        fields = '__all__'
