from django.contrib import admin

from applications.news.forms import CustomerNewsForm
from applications.news.models import CustomerNews


@admin.register(CustomerNews)
class CustomerNewsAdmin(admin.ModelAdmin):
    form = CustomerNewsForm
    readonly_fields = (
        'published',
        'notified_time',
    )
