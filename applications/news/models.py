from django.db import models
from django_extensions.db.models import TimeStampedModel


class CustomerNews(TimeStampedModel):
    title = models.CharField('title', max_length=255)
    body = models.TextField('body')
    published = models.BooleanField('news is published', default=False)
    notified_time = models.DateTimeField('notified time', null=True, blank=True)
    customers = models.ManyToManyField('users.User', limit_choices_to={'is_customer': True}, blank=True)
    public = models.BooleanField('public', help_text='news is available to everyone', default=False)

    class Meta:
        verbose_name = 'News for customers'
        verbose_name_plural = 'News for customers'

    def __str__(self):
        return self.title


class CustomerNewsViewLog(TimeStampedModel):
    customer = models.ForeignKey('users.Customer', on_delete=models.CASCADE, verbose_name='news_logs')
    news = models.ForeignKey('news.CustomerNews', on_delete=models.CASCADE, verbose_name='views')
