class AdminPermissionRequireMixin:
    """Administrators are the only ones who can add/change/delete data."""
    def has_add_permission(self, request):
        return request.user.is_administrator

    def has_delete_permission(self, request, obj=None):
        return request.user.is_administrator

    def has_change_permission(self, request, obj=None):
        return request.user.is_administrator

    def has_view_permission(self, request, obj=None):
        return True


class ReadOnlyPermissionAdminMixin:
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return True
