def split_display_name(display_name):
    display_name = display_name.strip('\u200c').split()
    first_name = display_name[0][:30]  # first_name max len is 30 characters
    last_name = ' '.join(display_name[1:])
    return [first_name, last_name]
