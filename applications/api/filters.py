import rest_framework_filters as filters
from rest_framework.filters import SearchFilter

from applications.treks.models import Tour


class CustomSchemaSearchFilter(SearchFilter):
    def get_schema_fields(self, view):
        self.search_description = "Fields for searching: " + ", ".join(view.search_columns)
        return super().get_schema_fields(view)


class TourFilter(filters.FilterSet):
    sights__id = filters.CharFilter(field_name='sights__id', method='filter_in_sights')
    opportunities__id = filters.CharFilter(field_name='opportunities__id', method='filter_in_opportunities')
    categories__title = filters.CharFilter(field_name='categories__title', method='filter_in_categories')

    class Meta:
        model = Tour
        fields = {
            'complexity': ['exact', 'in'],
            'distance': ['gt', 'gte', 'lt', 'lte'],
            'height': ['gt', 'gte', 'lt', 'lte'],
            'price': ['gt', 'gte', 'lt', 'lte'],
        }

    # noinspection PyMethodMayBeStatic
    def filter_in_sights(self, qs, name, value):
        try:
            values = value.split(',')
            qs = qs.filter(sights__id__in=values)
        except ValueError:
            pass
        return qs

    # noinspection PyMethodMayBeStatic
    def filter_in_opportunities(self, qs, name, value):
        try:
            values = value.split(',')
            qs = qs.filter(opportunities__id__in=values)
        except ValueError:
            pass
        return qs

    # noinspection PyMethodMayBeStatic
    def filter_in_categories(self, qs, name, value):
        values = value.split(',')
        return qs.filter(categories__title__in=values)
