from django.urls import path

from applications.api.views import company
from applications.api.views import guide
from applications.api.views.customer import *

customer_patterns = [
    path('profile/', ProfileView.as_view(), name='customer-profile'),
    path('profile/<int:pk>/', ProfileDeleteView.as_view(), name='customer-profile-delete'),
    path('tours/', TourListView.as_view(), name='customer-tour-list'),
    path('tours/<int:pk>/', TourDetailView.as_view(), name='customer-tour-detail'),
    path('tours/top/', TourTopView.as_view(), name='customer-top-tours'),
    path('tours/filtered/', TourFilteredListView.as_view(), name='customer-tours-filtered-list'),
    path('tours/search/', TourSearchView.as_view(), name='customer-tours-search'),
    path('tours/favorites/', FavoriteTourView.as_view(), name='customer-favorite-tours'),
    path('tours/favorites/<int:pk>/', FavoriteTourDestroyView.as_view(), name='customer-favorite-tour-destroy'),
    path('tours/sights/', TourSightsListView.as_view(), name='customer-tour-sights-list'),
    path('tours/opportunities/', TourOpportunityListView.as_view(), name='customer-tour-opportunities-list'),
    path('orders/', OrderCreateView.as_view(), name='customer-orders-create'),
    path('orders/<int:pk>/', OrderDetailView.as_view(), name='customer-order-detail'),
    path('orders/active/', OrderActiveListView.as_view(), name='customer-active-orders-list'),
    path('orders/history/', OrderHistoryListView.as_view(), name='customer-orders-history-list'),
    path('orders/feedback/', OrderFeedbackView.as_view(), name='customer-order-feedback'),
    path('news/', NewsListView.as_view(), name='customer-news-list'),
    path('news/<int:pk>/', NewsDetailView.as_view(), name='customer-news-detail'),
    path('categories/', CategoryView.as_view(), name='tour-categories-list'),
]

guide_patterns = [
    path('token-auth/', guide.LoginView.as_view(), name='token-auth'),
    path('token-refresh/', guide.RefreshIDToken.as_view(), name='token-refresh'),
    path('profile/', guide.ProfileView.as_view(), name='guide-profile'),
    path('tours/', guide.TourListView.as_view(), name='guide-tour-list'),
    path('tours/<int:pk>/', guide.TourDetailView.as_view(), name='guide-tour-detail'),
    path('tours/<int:tour_id>/orders/', guide.TourOrderView.as_view(), name='guide-tour-order-list'),
    path('orders/<int:pk>/', guide.OrderDetailView.as_view(), name='guide-order-detail'),
    path('areas/information/', guide.AreaInformationListView.as_view(), name='guide-area-information-list'),
    path(
        'areas/information/<int:pk>/', guide.AreaInformationDetailView.as_view(), name='guide-area-information-detail',
    ),
]

tour_company_patterns = [
    path('<int:company_id>/feedbacks/', company.CompanyFeedbackView.as_view(), name='company-feedback-list'),
    path('profile/<int:pk>/', company.CompanyProfileView.as_view(), name='company-profile'),
]
