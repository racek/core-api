from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import APIException


class AlreadyExistsError(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = _('Already exists.')
    default_code = 'error'


class TimeExpiredError(APIException):
    status_code = status.HTTP_406_NOT_ACCEPTABLE
    default_detail = _('Time expired.')
    default_code = 'error'


class AvailableTicketsError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Has not available seats.')
    default_code = 'error'


class NotPermittedError(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('Operation is not permitted.')
    default_code = 'error'


class NotImplementedMethod(APIException):
    status_code = status.HTTP_405_METHOD_NOT_ALLOWED
    default_detail = _('Http method is not allowed.')
    default_code = 'error'


class TourDoesNotHaveGuideError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('The tour does not have a guide.')
    default_code = 'error'
