from drf_yasg import openapi

tour_id_param = openapi.Parameter('username', in_=openapi.IN_PATH, description='Tour ID', type=openapi.TYPE_NUMBER)

login_response_schema_dict = {
    "200": openapi.Response(
        description="Success",
        examples={
            "application/json": {
                "token": "XXX",
            }
        }),
    "403": openapi.Response(
        description="Not permitted",
        examples={
            "application/json": {
                "detail": "Account is not a guide / No active account found with the given credentials.",
            }
        }
    ),
}
