import uuid

from django.db.models import Q, Sum
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import generics, status, pagination
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from applications.api.exceptions import NotPermittedError, NotImplementedMethod
from applications.api.filters import TourFilter, CustomSchemaSearchFilter
from applications.api.permissions import IsCustomerUser
from applications.api.serializers.customer import (
    TourSerializer,
    TourDetailSerializer,
    FavoriteTourSerializer,
    FavoriteTourCreateSerializer,
    OrderCreateSerializer,
    CustomerNewsSerializer,
    CustomerProfileSerializer,
    OrderSerializer,
    TourSightsSerializer,
    TourOpportunitySerializer,
    CustomerOrderFeedbackSerializer,
)
from applications.news.models import CustomerNews, CustomerNewsViewLog
from applications.orders.models import Order, OrderFeedback
from applications.treks.models import Tour, CustomerFavoriteTour, TourSight, TourOpportunity, TourCategory
from applications.users.models import User


class NewsPagination(pagination.PageNumberPagination):
    page_size = 25
    page_size_query_param = 'page_size'
    max_page_size = 10000

    def get_paginated_response(self, data):
        return Response({
            'total_size': self.page.paginator.count,
            'news': data
        })


class ProfileView(generics.RetrieveUpdateAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = CustomerProfileSerializer
    queryset = User.objects.filter(is_customer=True)

    def get_object(self):
        return get_object_or_404(User, id=self.request.user.id)


class ProfileDeleteView(generics.RetrieveDestroyAPIView):
    permission_classes = [IsCustomerUser]
    queryset = User.objects.filter(is_customer=True, is_active=True)

    def get_object(self):
        user = get_object_or_404(User, id=self.kwargs.get('pk'))
        if user.id != self.request.user.id:
            raise PermissionDenied()
        return user

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_active = False
        instance.username = uuid.uuid4()
        instance.save(update_fields=["is_active", "username"])
        return Response(status=status.HTTP_204_NO_CONTENT)


class TourListView(generics.ListAPIView):
    """
    Returns tour list.
    - /tours/?category=running - return tours belongs to running category
    - /tours/?company_id=1 - return only company with 1 ID tours.

    """
    permission_classes = []
    serializer_class = TourSerializer
    queryset = Tour.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    def get_queryset(self):
        query = self.queryset.filter(departure_at__gte=timezone.now())
        category = self.request.query_params.get('category', '')
        if category:
            query = query.filter(categories__title__in=[category])
        company = self.request.query_params.get('company_id')
        if company:
            query = query.filter(owner=company)
        return query

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'tours': response.data}
        return response


class TourTopView(generics.ListAPIView):
    permission_classes = []
    queryset = Tour.objects.all()
    serializer_class = TourSerializer

    def get_queryset(self):
        return self.queryset.filter(departure_at__gte=timezone.now()).annotate(
            orders_seats_sum=Sum('order__seats'),
        ).order_by('-orders_seats_sum')

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'tours': response.data}
        return response


class TourFilteredListView(generics.ListAPIView):
    """
    Returns tours according to given query params.
    By complexity:
     - tours/filtered/?complexity=easy
     - tours/filtered/?complexity=medium
     - tours/filtered/?complexity=hard

    By distance:
     - tours/filtered/?distance__gt=0 (distance greater than 0)
     - tours/filtered/?distance__lt=1 (distance less than 1)

    By height:
     - tours/filtered/?height__gt=0 (height greater than 0)
     - tours/filtered/?height__lt=1 (height less than 1)

    By sights__id:
     - tours/filtered/?sights__id=1 (1 is the ID of sight record)
     - tours/filtered/?sights__id=1,2

    By opportunities__id:
     - tours/filtered/?opportunities__id=1 (1 is the ID of opportunities record)
     - tours/filtered/?opportunities__id=1,2

    By categories:
     - tours/filtered/?categories__title=running (running is the title that has been got in the categories method)
     - tours/filtered/?categories__title=running,walking (walking - not WALKING or wAlkING)

     Example:
         - filtered/?height__gt=2&height__lt=5&distance__gt=100&
         distance__lt=250&complexity=easy&categories=running&opportunities__id=1,2
    """
    permission_classes = []
    queryset = Tour.objects.all()
    serializer_class = TourSerializer
    filter_class = TourFilter

    def get_queryset(self):
        return self.queryset.filter(departure_at__gte=timezone.now())

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'tours': response.data}
        return response


class TourSearchView(generics.ListAPIView):
    permission_classes = []
    queryset = Tour.objects.all()
    serializer_class = TourSerializer
    search_fields = [
        'title',
        'owner__title',
    ]
    # search columns needs only for Swagger.
    search_columns = ['title', 'owner (title)']
    filter_backends = (
        CustomSchemaSearchFilter,
    )

    def get_queryset(self):
        return self.queryset.filter(departure_at__gte=timezone.now())

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'tours': response.data}
        return response


class TourDetailView(generics.RetrieveAPIView):
    permission_classes = []
    serializer_class = TourDetailSerializer
    queryset = Tour.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    def get_object(self):
        return get_object_or_404(Tour, pk=self.kwargs.get('pk'))


class FavoriteTourView(generics.CreateAPIView, generics.ListAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = FavoriteTourSerializer
    queryset = CustomerFavoriteTour.objects.all()

    def get_queryset(self):
        return self.queryset.filter(customer=self.request.user)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'tours': response.data}
        return response

    def create(self, request, *args, **kwargs):
        serializer = FavoriteTourCreateSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        tour = TourDetailSerializer(serializer.instance.tour, context={'user': request.user})
        return Response(tour.data, status=status.HTTP_201_CREATED, headers=headers)


class FavoriteTourDestroyView(generics.UpdateAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = FavoriteTourSerializer
    queryset = CustomerFavoriteTour.objects.all()

    def get_object(self):
        return get_object_or_404(CustomerFavoriteTour, tour_id=self.kwargs.get('pk'), customer=self.request.user)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = TourDetailSerializer(instance.tour, context={'user': request.user})
        instance.delete()
        return Response(serializer.data)


class TourSightsListView(generics.ListAPIView):
    """
    Returns existing tour's sights (waterfall, lake, beach)
    """
    permission_classes = []
    serializer_class = TourSightsSerializer
    queryset = TourSight.objects.all()

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'sights': response.data}
        return response


class TourOpportunityListView(generics.ListAPIView):
    """
    Returns existing tour's opportunities (with a dog, with a child, asphalt road)
    """
    permission_classes = []
    serializer_class = TourOpportunitySerializer
    queryset = TourOpportunity.objects.all()

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'opportunities': response.data}
        return response


class OrderCreateView(generics.CreateAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = OrderCreateSerializer
    queryset = Order.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        order = OrderSerializer(serializer.instance, context={'user': request.user})
        return Response(order.data, status=status.HTTP_201_CREATED, headers=headers)


class OrderDetailView(generics.RetrieveUpdateAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get_object(self):
        return get_object_or_404(Order, pk=self.kwargs.get('pk'))

    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        _status = request.data.get('status')
        if _status and int(_status) == Order.CANCELED and not instance.is_cancelable:
            raise NotPermittedError('Departure at is coming soon. Cancel operation is not allowed.')
        return self.partial_update(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        raise NotImplementedMethod()


class OrderActiveListView(generics.ListAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get_queryset(self):
        return self.queryset.active().filter(buyer=self.request.user)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'orders': response.data}
        return response


class OrderHistoryListView(generics.ListAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get_queryset(self):
        return self.queryset.filter(
            Q(tour__departure_at__lt=timezone.now()) | Q(status__in=[Order.CANCELED, Order.USED]),
            buyer=self.request.user,
        )

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'orders': response.data}
        return response


class OrderFeedbackView(generics.CreateAPIView):
    """
    Add feedback to order (tour).
    - {'order': Order.ID, 'tour_score': 2, 'guide_score': 1, 'message': 'test message'}
    """
    permission_classes = [IsCustomerUser]
    serializer_class = CustomerOrderFeedbackSerializer
    queryset = OrderFeedback.objects.all()


class NewsListView(generics.ListAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = CustomerNewsSerializer
    queryset = CustomerNews.objects.all()
    pagination_class = NewsPagination

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({'request': self.request})
        return context

    def get_queryset(self):
        return self.queryset.filter(Q(public=True) | Q(customers=self.request.user)).order_by('-created')


class NewsDetailView(generics.RetrieveUpdateAPIView):
    permission_classes = [IsCustomerUser]
    serializer_class = CustomerNewsSerializer
    queryset = CustomerNews.objects.all()

    def get_object(self):
        return get_object_or_404(CustomerNews, pk=self.kwargs.get('pk'))

    def patch(self, request, *args, **kwargs):
        # Mark the news as viewed
        news = self.get_object()
        CustomerNewsViewLog.objects.get_or_create(customer=request.user, news=news)
        return Response(status=status.HTTP_201_CREATED)


class CategoryView(generics.ListAPIView):
    """
    Allow any to get existing tour categories.
    """
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        categories = TourCategory.objects.values_list('title', flat=True)
        return Response({'categories': list(categories)})
