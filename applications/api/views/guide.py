from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, generics
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from applications.api.permissions import IsGuideUser
from applications.api.schemes.guide import login_response_schema_dict
from applications.api.serializers.guide import (
    GuideRefreshTokenSerializer,
    GuideLoginSerializer,
    TourSerializer,
    TourDetailSerializer,
    OrderSerializer,
    OrderSmallSerializer,
    UserSerializer,
    AreaInformationSerializer,
)
from applications.core.models import AreaInformation
from applications.orders.models import Order
from applications.treks.models import Tour

User = get_user_model()


class LoginView(GenericAPIView):
    """
    Check the credentials and return the REST Token if the credentials are valid and authenticated.
    Calls Django Auth login method to register User ID in Django session framework
    Accept the following POST parameters: username, password
    Return the REST Framework Token Object's key.
    """
    permission_classes = (AllowAny,)
    authentication_classes = ()
    serializer_class = GuideLoginSerializer

    @swagger_auto_schema(
        manual_parameters=[],
        security=[],
        responses=login_response_schema_dict,
        operation_id='Create token',
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class RefreshIDToken(GenericAPIView):
    """
    Returns a refreshed firebase ID token (with new expiration) based on existing custom token.
    https://stackoverflow.com/a/49290904/3840005
    """
    permission_classes = (AllowAny,)
    authentication_classes = ()
    serializer_class = GuideRefreshTokenSerializer

    @swagger_auto_schema(
        operation_id='Update expired token',
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class ProfileView(generics.RetrieveAPIView):
    permission_classes = [IsGuideUser]
    serializer_class = UserSerializer
    queryset = User.objects.filter(is_guide=True, is_customer=False)

    def get_object(self):
        return get_object_or_404(User, id=self.request.user.id)


class TourListView(generics.ListAPIView):
    permission_classes = [IsGuideUser]
    serializer_class = TourSerializer
    queryset = Tour.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    def get_queryset(self):
        return self.queryset.filter(guide=self.request.user)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'tours': response.data}
        return response


class TourDetailView(generics.RetrieveAPIView, generics.UpdateAPIView):
    http_method_names = ['get', 'patch']
    permission_classes = [IsGuideUser]
    serializer_class = TourDetailSerializer
    queryset = Tour.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    def get_object(self):
        return get_object_or_404(Tour, pk=self.kwargs.get('pk'))

    def patch(self, request, *args, **kwargs):
        tour = self.get_object()
        _status = request.data.get('status')
        if _status and int(_status) == Tour.FINISHED:
            tour.order_set.filter(status=Order.CONFIRMED).update(status=Order.USED)
            tour.order_set.filter(status=Order.IDLE).update(status=Order.MISSED)
            tour.finisher = request.user
            tour.actual_finished_at = timezone.now()
            tour.save()
        return self.partial_update(request, *args, **kwargs)


class TourOrderView(generics.ListAPIView):
    permission_classes = [IsGuideUser]
    serializer_class = OrderSerializer
    queryset = Order.objects.active()

    def get_queryset(self):
        return self.queryset.filter(tour_id=self.kwargs.get('tour_id'))

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'orders': response.data}
        return response


class OrderDetailView(RetrieveModelMixin, UpdateModelMixin, GenericAPIView):
    """
    Retrieve and update customer order data.
    - {'status': 2} - mark the order as confirmed
    """
    permission_classes = [IsGuideUser]
    queryset = Order.objects.all()
    serializer_class = OrderSmallSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class AreaInformationListView(generics.ListAPIView):
    permission_classes = [IsGuideUser]
    serializer_class = AreaInformationSerializer
    queryset = AreaInformation.objects.values('id', 'title', 'text')

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'information': response.data}
        return response


class AreaInformationDetailView(generics.RetrieveAPIView):
    permission_classes = [IsGuideUser]
    serializer_class = AreaInformationSerializer
    queryset = AreaInformation.objects.values('id', 'title', 'text')
