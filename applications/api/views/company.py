from django.shortcuts import get_object_or_404
from rest_framework import generics

from applications.api.serializers.company import CompanyProfileSerializer, CompanyFeedbackSerializer
from applications.orders.models import OrderFeedback
from applications.users.models import TourGroup


class CompanyProfileView(generics.RetrieveAPIView):
    permission_classes = []
    serializer_class = CompanyProfileSerializer
    queryset = TourGroup.objects.all()

    def get_object(self):
        return get_object_or_404(TourGroup, id=self.kwargs.get('pk'))


class CompanyFeedbackView(generics.ListAPIView):
    permission_classes = []
    serializer_class = CompanyFeedbackSerializer
    queryset = OrderFeedback.objects.all()

    def get_queryset(self):
        return self.queryset.filter(tour_company=self.kwargs.get('company_id'))

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {'feedbacks': response.data}
        return response
