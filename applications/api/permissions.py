from rest_framework import permissions


class IsCustomerUser(permissions.BasePermission):
    """
    Allows access only to customer users.
    """

    def has_permission(self, request, view):
        return not request.user.is_anonymous and request.user.is_customer


class IsGuideUser(permissions.BasePermission):
    """
    Allows access only to guide users.
    """

    def has_permission(self, request, view):
        return not request.user.is_anonymous and request.user.is_guide
