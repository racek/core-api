import requests
from django.conf import settings
from django.contrib.auth import authenticate
from django.db.models import Sum
from django.utils.encoding import smart_str
from firebase_admin import auth
from rest_framework import serializers
from django.db.models import Avg

from applications.api.exceptions import NotPermittedError
from applications.api.serializers import AdditionalPassengerSerializer
from applications.orders.models import Order
from applications.treks.models import Tour
from applications.users.models import User


class UserSerializer(serializers.ModelSerializer):
    fullname = serializers.SerializerMethodField()
    avatar_url = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id',
            'fullname',
            'avatar_url',
            'phone_number',
            'rating',
            'city',
        ]

    # noinspection PyMethodMayBeStatic
    def get_rating(self, user):
        return user.feedbacks.aggregate(average_rating=Avg('guide_score'))['average_rating']

    # noinspection PyMethodMayBeStatic
    def get_fullname(self, user):
        return user.get_full_name()

    # noinspection PyMethodMayBeStatic
    def get_avatar_url(self, user):
        if user.is_customer:
            return user.fb_photo_url
        return user.guide_avatar.url


class GuideLoginSerializer(serializers.Serializer):
    username_field = User.USERNAME_FIELD
    password = serializers.CharField(style={'input_type': 'password'}, write_only=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields[self.username_field] = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(**{
            self.username_field: attrs[self.username_field],
            'password': attrs['password'],
        }, request=self.context.get('request'))
        if user is None or not user.is_active:
            raise NotPermittedError('No active account found with the given credentials.')
        if not user.is_guide:
            raise NotPermittedError('Account is not a guide.')

        try:
            custom_token = auth.create_custom_token(user.username, {'is_guide': True})
        except auth.TokenSignError as ex:
            raise serializers.ValidationError(ex)

        return {'token': smart_str(custom_token)}


class GuideRefreshTokenSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate(self, attrs):
        token = attrs['token']

        # https://firebase.google.com/docs/reference/rest/auth/#section-verify-custom-token
        url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=" \
              + settings.FIREBASE_API_KEY
        payload = {
            "token": token,
            "returnSecureToken": True
        }
        resp = requests.post(url, json=payload)
        if resp.status_code != 200:
            raise serializers.ValidationError("Service unavailable")

        return {"token": resp.json()["idToken"]}


class TourSerializer(serializers.ModelSerializer):
    image_urls = serializers.SerializerMethodField()
    owner_icon_url = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    already_booked_seats = serializers.SerializerMethodField()
    location_city = serializers.SerializerMethodField()
    location_country = serializers.SerializerMethodField()

    class Meta:
        ref_name = 'Guide tour'
        model = Tour
        fields = [
            'id',
            'title',
            'departure_at',
            'arrival_at',
            'pickup_time',
            'pickup_address',
            'price',
            'currency',
            'image_urls',
            'owner_icon_url',
            'tags',
            'complexity',
            'duration',
            'categories',
            'available_tickets_count',
            'already_booked_seats',
            'location_city',
            'location_country',
        ]

    # noinspection PyMethodMayBeStatic
    def get_image_urls(self, tour):
        return [record.image.url for record in tour.images.all()]

    # noinspection PyMethodMayBeStatic
    def get_owner_icon_url(self, tour):
        return tour.owner.icon.url

    # noinspection PyMethodMayBeStatic
    def get_tags(self, tour):
        return tour.tags.values_list('name', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_already_booked_seats(self, tour):
        already_booked_seats = Order.objects.filter(tour=tour).active().aggregate(Sum('seats'))['seats__sum']
        if not already_booked_seats:
            return 0
        return already_booked_seats

    # noinspection PyMethodMaybeStatic
    def get_location_city(self, tour):
        return tour.location.city if tour.location else ''

    # noinspection PyMethodMaybeStatic
    def get_location_country(self, tour):
        return tour.location.country if tour.location else ''


class TourDetailSerializer(serializers.ModelSerializer):
    image_urls = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    guide = UserSerializer(read_only=True)
    already_booked_seats = serializers.SerializerMethodField()
    owner_icon_url = serializers.SerializerMethodField()
    location_city = serializers.SerializerMethodField()
    location_country = serializers.SerializerMethodField()

    class Meta:
        ref_name = 'Guide tour details'
        model = Tour
        fields = [
            'id',
            'title',
            'departure_at',
            'arrival_at',
            'pickup_time',
            'pickup_address',
            'description',
            'duration',
            'price',
            'currency',
            'image_urls',
            'tags',
            'complexity',
            'guide',
            'owner_icon_url',
            'height',
            'distance',
            'categories',
            'available_tickets_count',
            'already_booked_seats',
            'status',
            'location_city',
            'location_country',
        ]

    # noinspection PyMethodMayBeStatic
    def get_image_urls(self, tour):
        return [record.image.url for record in tour.images.all()]

    # noinspection PyMethodMayBeStatic
    def get_tags(self, tour):
        return tour.tags.values_list('name', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_owner_icon_url(self, tour):
        return tour.owner.icon.url

    # noinspection PyMethodMayBeStatic
    def get_already_booked_seats(self, tour):
        already_booked_seats = Order.objects.filter(tour=tour).active().aggregate(Sum('seats'))['seats__sum']
        if not already_booked_seats:
            return 0
        return already_booked_seats

    # noinspection PyMethodMaybeStatic
    def get_location_city(self, tour):
        return tour.location.city if tour.location else ''

    # noinspection PyMethodMaybeStatic
    def get_location_country(self, tour):
        return tour.location.country if tour.location else ''


class OrderSerializer(serializers.ModelSerializer):
    buyer = UserSerializer(read_only=True)

    class Meta:
        ref_name = 'Guide tour order'
        model = Order
        fields = [
            'id',
            'code',
            'seats',
            'buyer',
            'status',
        ]


class OrderSmallSerializer(serializers.ModelSerializer):
    buyer = UserSerializer(read_only=True)
    additional_passengers = AdditionalPassengerSerializer(many=True, read_only=True)

    class Meta:
        ref_name = 'Guide order small'
        model = Order
        fields = [
            'id',
            'status',
            'code',
            'buyer',
            'additional_passengers',
            'seats',
        ]


class AreaInformationSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    description = serializers.CharField(source='text')
