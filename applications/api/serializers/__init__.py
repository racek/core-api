from rest_framework import serializers

from applications.orders.models import AdditionalPassenger


class AdditionalPassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalPassenger
        fields = [
            'fullname',
            'phone_number',
        ]

