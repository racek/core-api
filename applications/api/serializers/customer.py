from django.db.models import Sum
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from applications.api.exceptions import (
    AlreadyExistsError,
    TimeExpiredError,
    AvailableTicketsError,
    TourDoesNotHaveGuideError,
)
from applications.api.serializers import AdditionalPassengerSerializer
from applications.api.serializers.guide import UserSerializer
from applications.news.models import CustomerNews
from applications.orders.models import Order, OrderFeedback, AdditionalPassenger
from applications.treks.models import Tour, CustomerFavoriteTour, TourSight, TourOpportunity
from applications.users.models import User


class RoundingDecimalField(serializers.DecimalField):
    """
    DecimalField subclass that skips the precision validation.
    It does apply 'required' validation, and any other validation
    done by the parent drf.Field class.
    """

    def validate_precision(self, value):
        return value


class CustomerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'phone_number',
            'city',
            'language',
            'fb_photo_url',
        )


class TourSerializer(serializers.ModelSerializer):
    image_urls = serializers.SerializerMethodField()
    owner_icon_url = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    is_favorite = serializers.SerializerMethodField()
    order_id = serializers.SerializerMethodField()
    sights = serializers.SerializerMethodField()
    opportunities = serializers.SerializerMethodField()
    owner_id = serializers.IntegerField(source='owner.id')
    owner_title = serializers.CharField(source='owner.title')
    location_city = serializers.SerializerMethodField()
    location_country = serializers.SerializerMethodField()

    class Meta:
        ref_name = 'Customer tour'
        model = Tour
        fields = [
            'id',
            'title',
            'departure_at',
            'arrival_at',
            'pickup_address',
            'pickup_time',
            'price',
            'currency',
            'image_urls',
            'owner_icon_url',
            'tags',
            'complexity',
            'duration',
            'is_favorite',
            'order_id',
            'owner_title',
            'categories',
            'distance',
            'height',
            'sights',
            'opportunities',
            'owner_id',
            'location_city',
            'location_country',
        ]

    # noinspection PyMethodMayBeStatic
    def get_image_urls(self, tour):
        return [record.image.url for record in tour.images.all()]

    # noinspection PyMethodMayBeStatic
    def get_sights(self, tour):
        return tour.sights.values_list('title', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_opportunities(self, tour):
        return tour.opportunities.values_list('title', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_owner_icon_url(self, tour):
        return tour.owner.icon.url

    # noinspection PyMethodMayBeStatic
    def get_tags(self, tour):
        return tour.tags.values_list('name', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_is_favorite(self, tour):
        user = self.context.get('user')
        if not user or not user.is_authenticated:
            return False
        return user.favorite_tours.filter(tour=tour).exists()

    # noinspection PyMethodMaybeStatic
    def get_order_id(self, tour):
        """Returns order's ID if customer has purchased this tour."""
        user = self.context.get('user')
        if not user or not user.is_authenticated:
            return None
        orders = user.order_set.active().filter(tour=tour)
        if orders.exists():
            return orders.get(tour=tour).id

    # noinspection PyMethodMaybeStatic
    def get_location_city(self, tour):
        return tour.location.city if tour.location else ''

    # noinspection PyMethodMaybeStatic
    def get_location_country(self, tour):
        return tour.location.country if tour.location else ''


class TourDetailSerializer(serializers.ModelSerializer):
    image_urls = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    guide = UserSerializer(read_only=True)
    available_for_purchase_count = serializers.SerializerMethodField()
    is_favorite = serializers.SerializerMethodField()
    owner_icon_url = serializers.SerializerMethodField()
    owner_id = serializers.IntegerField(source='owner.id')
    categories = serializers.SerializerMethodField()
    location_city = serializers.SerializerMethodField()
    location_country = serializers.SerializerMethodField()

    class Meta:
        ref_name = 'Customer tour details'
        model = Tour
        fields = [
            'id',
            'title',
            'departure_at',
            'arrival_at',
            'pickup_address',
            'pickup_time',
            'description',
            'duration',
            'price',
            'currency',
            'image_urls',
            'tags',
            'complexity',
            'guide',
            'available_for_purchase_count',
            'is_favorite',
            'owner_icon_url',
            'height',
            'distance',
            'categories',
            'owner_id',
            'location_city',
            'location_country',
        ]

    # noinspection PyMethodMayBeStatic
    def get_image_urls(self, tour):
        return [record.image.url for record in tour.images.all()]

    # noinspection PyMethodMayBeStatic
    def get_tags(self, tour):
        return tour.tags.values_list('name', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_available_for_purchase_count(self, tour):
        booked_tickets = tour.order_set.active().aggregate(Sum('seats'))['seats__sum']
        if not booked_tickets:
            booked_tickets = 0
        return tour.available_tickets_count - booked_tickets

    # noinspection PyMethodMayBeStatic
    def get_is_favorite(self, tour):
        user = self.context['user']
        if not user or not user.is_authenticated:
            return False
        return user.favorite_tours.filter(tour=tour).exists()

    # noinspection PyMethodMayBeStatic
    def get_owner_icon_url(self, tour):
        return tour.owner.icon.url

    # noinspection PyMethodMayBeStatic
    def get_categories(self, tour):
        return tour.categories.values_list('title', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_location_city(self, tour):
        return tour.location.city if tour.location else ''

    # noinspection PyMethodMayBeStatic
    def get_location_country(self, tour):
        return tour.location.country if tour.location else ''


class FavoriteTourSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='tour.id')
    title = serializers.CharField(source='tour.title')
    departure_at = serializers.DateTimeField(source='tour.departure_at')
    arrival_at = serializers.DateTimeField(source='tour.arrival_at')
    pickup_address = serializers.CharField(source='tour.pickup_address')
    pickup_time = serializers.CharField(source='tour.pickup_time')
    price = serializers.IntegerField(source='tour.price')
    currency = serializers.CharField(source='tour.currency')
    image_urls = serializers.SerializerMethodField()
    owner_icon_url = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    complexity = serializers.CharField(source='tour.complexity')
    duration = serializers.IntegerField(source='tour.duration')
    is_favorite = serializers.SerializerMethodField()
    order_id = serializers.SerializerMethodField()
    categories = serializers.SerializerMethodField()

    class Meta:
        model = CustomerFavoriteTour
        fields = [
            'id',
            'title',
            'departure_at',
            'arrival_at',
            'pickup_address',
            'pickup_time',
            'price',
            'currency',
            'image_urls',
            'owner_icon_url',
            'tags',
            'complexity',
            'duration',
            'is_favorite',
            'order_id',
            'categories',
        ]

    # noinspection PyMethodMayBeStatic
    def get_image_urls(self, obj):
        return [record.image.url for record in obj.tour.images.all()]

    # noinspection PyMethodMayBeStatic
    def get_owner_icon_url(self, obj):
        return obj.tour.owner.icon.url

    # noinspection PyMethodMayBeStatic
    def get_tags(self, obj):
        return obj.tour.tags.values_list('name', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_categories(self, obj):
        return obj.tour.categories.values_list('title', flat=True)

    # noinspection PyMethodMayBeStatic
    def get_is_favorite(self, obj):
        return True

    # noinspection PyMethodMaybeStatic
    def get_order_id(self, tour):
        """Returns order's ID if customer has purchased this tour."""
        user = self.context.get('user')
        if not user or not user.is_authenticated:
            return None
        orders = user.order_set.active().filter(tour=tour)
        if orders.exists():
            return orders.get(tour=tour).id


class FavoriteTourCreateSerializer(serializers.ModelSerializer):
    customer = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = CustomerFavoriteTour
        fields = [
            'id',
            'tour',
            'customer',
        ]
        extra_kwargs = {
            'id': {'read_only': True},
        }

    def validate(self, attrs):
        tour = attrs['tour']
        customer = attrs['customer']
        if customer.favorite_tours.filter(tour=tour).exists():
            raise AlreadyExistsError('The tour is already in your favorites.')
        return attrs


class TourSightsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TourSight
        fields = [
            'id',
            'title',
        ]


class TourOpportunitySerializer(serializers.ModelSerializer):
    class Meta:
        model = TourOpportunity
        fields = [
            'id',
            'title',
        ]


class OrderSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='tour.title', read_only=True)
    departure_at = serializers.DateTimeField(source='tour.departure_at', read_only=True)
    arrival_at = serializers.DateTimeField(source='tour.arrival_at', read_only=True)
    pickup_address = serializers.CharField(source='tour.pickup_address', read_only=True)
    pickup_time = serializers.TimeField(source='tour.pickup_time', read_only=True)
    price = serializers.DecimalField(source='tour.price', decimal_places=2, max_digits=17, read_only=True)
    currency = serializers.CharField(source='tour.currency', read_only=True)
    image_urls = serializers.SerializerMethodField()
    owner_icon_url = serializers.CharField(source='tour.owner.icon.url', read_only=True)
    complexity = serializers.CharField(source='tour.complexity', read_only=True)
    tags = serializers.SerializerMethodField()
    description = serializers.CharField(source='tour.description', read_only=True)
    duration = serializers.IntegerField(source='tour.duration', read_only=True)
    guide = UserSerializer(source='tour.guide', read_only=True)
    distance = serializers.IntegerField(source='tour.distance', read_only=True)
    height = serializers.IntegerField(source='tour.height', read_only=True)
    tour_id = serializers.IntegerField(source='tour.id')
    owner_id = serializers.IntegerField(source='tour.owner.id', read_only=True)
    owner_title = serializers.CharField(source='tour.owner.title', read_only=True)
    additional_passengers = AdditionalPassengerSerializer(many=True, read_only=True)
    rating = serializers.SerializerMethodField()
    location_city = serializers.SerializerMethodField()
    location_country = serializers.SerializerMethodField()
    available_for_purchase_count = serializers.SerializerMethodField()

    class Meta:
        ref_name = 'Customer order'
        model = Order
        fields = [
            'id',
            'status',
            'title',
            'departure_at',
            'arrival_at',
            'pickup_address',
            'pickup_time',
            'price',
            'currency',
            'image_urls',
            'owner_icon_url',
            'tags',
            'complexity',
            'code',
            'description',
            'duration',
            'guide',
            'distance',
            'height',
            'seats',
            'tour_id',
            'owner_id',
            'owner_title',
            'additional_passengers',
            'rating',
            'location_city',
            'location_country',
            'created',
            'available_for_purchase_count',
        ]

    # noinspection PyMethodMayBeStatic
    def get_image_urls(self, order):
        return [record.image.url for record in order.tour.images.all()]

    # noinspection PyMethodMayBeStatic
    def get_tags(self, order):
        return [tag.name for tag in order.tour.tags.all()]

    # noinspection PyMethodMayBeStatic
    def get_rating(self, order):
        feedback = order.feedbacks.last()
        if feedback:
            return feedback.tour_score
        return None

    # noinspection PyMethodMayBeStatic
    def get_location_city(self, order):
        location = order.tour.location
        return location.city if location else ''

    # noinspection PyMethodMayBeStatic
    def get_location_country(self, order):
        location = order.tour.location
        return location.country if location else ''

    # noinspection PyMethodMayBeStatic
    def get_available_for_purchase_count(self, order):
        return order.tour.get_available_for_purchase_count()


class OrderCreateSerializer(serializers.ModelSerializer):
    buyer = serializers.HiddenField(default=serializers.CurrentUserDefault())
    additional_passengers = AdditionalPassengerSerializer(many=True, required=False)

    class Meta:
        model = Order
        fields = [
            'id',
            'buyer',
            'tour',
            'seats',
            'additional_passengers',
        ]
        extra_kwargs = {
            'id': {'read_only': True},
        }

    def validate(self, attrs):
        # The method should be expand when there will used some fares, promo-codes etc.
        tour = attrs['tour']
        seats = attrs['seats']
        attrs['price'] = tour.price
        attrs['code'] = Order.generate_code(tour.id)

        already_booked_seats = Order.objects.filter(tour=tour).active().aggregate(Sum('seats'))['seats__sum']
        if not already_booked_seats:
            already_booked_seats = 0

        if tour.available_tickets_count < already_booked_seats + seats:
            raise AvailableTicketsError(_('No available tickets.'))

        if Order.objects.active().filter(buyer=attrs['buyer'], tour=attrs['tour']).exists():
            raise AlreadyExistsError(_('Tour already booked.'))

        if tour.departure_at < timezone.now():
            raise TimeExpiredError(_('Departure time expired.'))

        if seats > tour.max_seats_per_booking:
            raise AvailableTicketsError(_('The seat limit for bookings is exceeded.'))

        return attrs

    def create(self, validated_data):
        additional_passengers = validated_data.pop('additional_passengers', [])
        order = Order.objects.create(**validated_data)
        for passenger in additional_passengers:
            AdditionalPassenger.objects.create(order=order, **passenger)
        return order


class CustomerNewsSerializer(serializers.ModelSerializer):
    viewed = serializers.SerializerMethodField()

    class Meta:
        model = CustomerNews
        fields = (
            'id',
            'title',
            'body',
            'viewed',
            'created',
        )

    def get_viewed(self, news):
        request = self.context.get('request')
        return news.customernewsviewlog_set.filter(customer=request.user).exists()


class CustomerOrderFeedbackSerializer(serializers.ModelSerializer):
    customer = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = OrderFeedback
        fields = (
            'customer',
            'order',
            'tour_score',
            'guide_score',
            'message',
            'guide',
        )
        extra_kwargs = {
            'guide': {'read_only': True},
        }

    def validate(self, attrs):
        order = attrs['order']
        if OrderFeedback.objects.filter(order=order).exists():
            raise AlreadyExistsError('The feedback already exists.')
        if not order.tour.guide:
            raise TourDoesNotHaveGuideError()

        attrs['guide'] = order.tour.guide
        attrs['tour_company'] = order.tour.owner
        return attrs
