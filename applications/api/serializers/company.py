from django.db.models import Avg
from rest_framework import serializers

from applications.api.serializers.customer import CustomerProfileSerializer
from applications.orders.models import OrderFeedback
from applications.users.models import TourGroup


class CompanyProfileSerializer(serializers.ModelSerializer):
    icon_url = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()

    class Meta:
        model = TourGroup
        fields = (
            'id',
            'title',
            'icon_url',
            'email',
            'description',
            'rating',
        )

    # noinspection PyMethodMayBeStatic
    def get_icon_url(self, obj):
        return obj.icon.url

    # noinspection PyMethodMayBeStatic
    def get_rating(self, obj):
        rating = obj.feedbacks.aggregate(Avg('tour_score'))['tour_score__avg']
        return round(rating, 2) if rating else None


class CompanyFeedbackSerializer(serializers.ModelSerializer):
    customer = CustomerProfileSerializer()

    class Meta:
        model = OrderFeedback
        fields = (
            'customer',
            'created',
            'message',
            'tour_score',
            'reaction_message',
        )
