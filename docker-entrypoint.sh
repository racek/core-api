#!/usr/bin/env bash
# docker-entrypoint.sh

set -o errexit
set -o pipefail
set -o nounset


bash "wait-for-postgres.sh"

# Run python specific scripts:
python ./manage.py collectstatic --noinput

echo "Starting web server."
# "Running any commands passed to this script from CMD or command..."
exec "$@"
