import pytest
from django.contrib.auth import get_user_model

from applications.users.models import Customer


@pytest.mark.django_db
def test_should_get_customer_record():
    get_user_model().objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword', is_customer=True)
    assert Customer.objects.count() == 1
