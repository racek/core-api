import pytest
from django.urls import reverse

from applications.orders.models import Order, AdditionalPassenger
from tests.conftest import OrderFactory, GuideFactory


@pytest.mark.django_db
def test_guide_can_update_customer_order_status(api_client):
    order = OrderFactory(status=Order.IDLE)
    guide = GuideFactory()
    api_client.login(username=guide.username, password='fooBar')

    url = reverse('guide-order-detail', args=(order.id, ))
    response = api_client.patch(url, data={'status': Order.CONFIRMED})

    assert response.status_code == 200
    order.refresh_from_db()
    assert order.status == Order.CONFIRMED


@pytest.mark.django_db
def test_guide_can_retrieve_order_details(api_client):
    guide = GuideFactory()
    order = OrderFactory(seats=2)
    AdditionalPassenger.objects.create(order=order, fullname='Foo Bar', phone_number='+996555222111')

    api_client.login(username=guide.username, password='fooBar')
    url = reverse('guide-order-detail', args=(order.id, ))
    response = api_client.get(url)

    assert response.json()['id'] == order.id
    assert response.json()['code'] == order.code
    assert response.json()['seats'] == 2
    assert response.json()['additional_passengers'] == [
        {
            'fullname': 'Foo Bar',
            'phone_number': '+996555222111',
        }
    ]
