import pytest
from django.urls import reverse
from rest_framework import status

from applications.treks.models import TourCategory


@pytest.mark.django_db
def test_should_return_existed_categories(api_client):
    TourCategory.objects.create(title='trekking')
    TourCategory.objects.create(title='running')

    url = reverse('tour-categories-list')
    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert sorted(response.json()['categories']) == sorted(['trekking', 'running'])
