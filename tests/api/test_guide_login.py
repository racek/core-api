import pytest
from django.urls import reverse
from rest_framework import status

from tests.conftest import CustomerFactory, GuideFactory


@pytest.mark.django_db
def test_should_raise_error_when_user_is_not_activated(api_client):
    CustomerFactory(username='foo', password='bar', is_active=False)
    url = reverse('token-auth')

    response = api_client.post(url, data={'username': 'foo', 'password': 'bar'})

    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json()['detail'] == 'No active account found with the given credentials.'


@pytest.mark.django_db
def test_should_raise_error_when_user_is_not_guide(api_client):
    CustomerFactory(username='foo', password='bar')
    url = reverse('token-auth')

    response = api_client.post(url, data={'username': 'foo', 'password': 'bar'})

    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json()['detail'] == 'Account is not a guide.'


@pytest.mark.django_db
def test_should_return_custom_token_when_everything_is_ok(api_client, mocker):
    GuideFactory(username='foo', password='bar')
    url = reverse('token-auth')

    mocker.patch('applications.api.serializers.guide.auth.create_custom_token', return_value='FooBarToken')
    response = api_client.post(url, data={'username': 'foo', 'password': 'bar'})

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['token'] == 'FooBarToken'
