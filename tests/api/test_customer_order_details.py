from datetime import datetime

import pytest
from django.urls import reverse
from freezegun import freeze_time
from rest_framework import status

from applications.orders.models import Order, AdditionalPassenger
from tests.conftest import CustomerFactory, OrderFactory


@pytest.mark.django_db
def test_should_return_order_details(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    order = OrderFactory()
    AdditionalPassenger.objects.create(order=order, fullname='Foo Bar', phone_number='+996555222111')

    url = reverse('customer-order-detail', args=(order.id, ))
    response = api_client.get(url)

    fields = response.json().keys()
    assert response.json()['id'] == order.id
    assert response.json()['title'] == order.tour.title
    assert response.json()['code'] == order.code
    assert response.json()['additional_passengers'] == [
        {
            'fullname': 'Foo Bar',
            'phone_number': '+996555222111',
        }
    ]
    assert 'location_city' in fields
    assert 'location_country' in fields
    assert 'owner_title' in fields
    assert 'created' in fields


@pytest.mark.django_db
def test_available_to_cancel_order_before_deadline(api_client):
    order = OrderFactory(
        tour__departure_at=datetime(2021, 4, 26, 13, 30),
        tour__cancel_deadline=30,
    )
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-order-detail', args=(order.id, ))

    with freeze_time('2021-04-26 12:59:00'):
        response = api_client.patch(url, data={'status': Order.CANCELED})

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['tour_id'] == order.tour.id
    order.refresh_from_db()
    assert order.status == Order.CANCELED


@pytest.mark.django_db
def test_should_not_available_to_cancel_order_after_deadline(api_client):
    order = OrderFactory(
        tour__departure_at=datetime(2021, 4, 26, 13, 30),
        tour__cancel_deadline=30,
    )
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-order-detail', args=(order.id, ))

    with freeze_time('2021-04-26 13:01:00'):
        response = api_client.patch(url, data={'status': Order.CANCELED})

    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json()['detail'] == 'Departure at is coming soon. Cancel operation is not allowed.'


@pytest.mark.django_db
def test_put_method_is_not_allowed(api_client):
    order = OrderFactory()
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-order-detail', args=(order.id, ))

    response = api_client.put(url, data={'status': Order.CANCELED})

    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

