import pytest
from django.urls import reverse

from tests.conftest import OrderFeedbackFactory


@pytest.mark.django_db
def test_return_company_feedbacks(api_client):
    feedback = OrderFeedbackFactory()

    url = reverse('company-feedback-list', args=(feedback.tour_company.id, ))
    response = api_client.get(url)

    assert response.status_code == 200
    assert len(response.json()['feedbacks']) == 1
    assert response.json()['feedbacks'][0]['message'] == feedback.message
    assert response.json()['feedbacks'][0]['tour_score'] == feedback.tour_score
    assert response.json()['feedbacks'][0]['customer']['id'] == feedback.customer.id
