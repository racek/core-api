import pytest
from django.urls import reverse

from applications.users.models import User
from tests.conftest import CustomerFactory


pytestmark = pytest.mark.django_db


def test_customer_can_get_own_profile(api_client):
    customer = CustomerFactory(first_name='Foo', last_name='Bar', city=User.BISHKEK, language=User.RUSSIAN)
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-profile')

    response = api_client.get(url)

    assert response.status_code == 200
    assert response.json()['id'] == customer.id
    assert response.json()['first_name'] == 'Foo'
    assert response.json()['last_name'] == 'Bar'


def test_customer_can_partial_update_profile(api_client):
    customer = CustomerFactory(first_name='Foo', last_name='Bar', city=User.BISHKEK, language=User.RUSSIAN)
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-profile')

    response = api_client.patch(url, data={'language': User.ENGLISH})

    assert response.status_code == 200
    customer.refresh_from_db()
    assert customer.language == User.ENGLISH


def test_customer_can_deactivate_profile(api_client):
    customer = CustomerFactory(username="fooName")
    api_client.login(username=customer.username, password='fooBar')

    url = reverse('customer-profile-delete', kwargs={'pk': customer.pk})
    response = api_client.delete(url)

    assert response.status_code == 204
    customer.refresh_from_db()
    assert not customer.is_active
    assert not customer.username == "fooName"


def test_customer_can_not_deactivate_another_profile(api_client):
    customer = CustomerFactory()
    another_customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')

    url = reverse('customer-profile-delete', kwargs={'pk': another_customer.pk})
    response = api_client.delete(url)

    assert response.status_code == 403
    another_customer.refresh_from_db()
    assert another_customer.is_active
