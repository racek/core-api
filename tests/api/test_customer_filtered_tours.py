import pytest
from django.urls import reverse

from applications.treks.models import TourSight, TourOpportunity, TourCategory
from tests.conftest import TourFactory, CustomerFactory


@pytest.mark.django_db
def test_should_return_tours_according_to_distance(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    TourFactory(title='Foo Bar #1', distance=125)
    TourFactory(title='Foo Bar #2', distance=150)
    TourFactory(title='Foo Bar #3', distance=175)
    TourFactory(title='Foo Bar #4', distance=200)

    url = reverse('customer-tours-filtered-list')
    params = '?distance__gt=149&distance__lt=200'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    assert sorted([tour['title'] for tour in response.json()['tours']]) == sorted(['Foo Bar #2', 'Foo Bar #3'])


@pytest.mark.django_db
def test_should_return_tours_according_to_height(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    TourFactory(title='Foo Bar #1', height=1250)
    TourFactory(title='Foo Bar #2', height=1500)
    TourFactory(title='Foo Bar #3', height=1750)
    TourFactory(title='Foo Bar #4', height=2000)

    url = reverse('customer-tours-filtered-list')
    params = '?height__gt=1490&height__lt=2000'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    assert sorted([tour['title'] for tour in response.json()['tours']]) == sorted(['Foo Bar #2', 'Foo Bar #3'])


@pytest.mark.django_db
def test_should_return_tours_according_to_complexity(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    TourFactory(title='Foo Bar #1', complexity='easy')
    TourFactory(title='Foo Bar #2', complexity='medium')
    TourFactory(title='Foo Bar #3', complexity='hard')

    url = reverse('customer-tours-filtered-list')
    params = '?complexity=medium'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['title'] == 'Foo Bar #2'


@pytest.mark.django_db
def test_should_return_tours_according_to_sights(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    tour = TourFactory(title='Foo Bar #1')
    tour_2 = TourFactory(title='Foo Bar #2')
    tour_3 = TourFactory(title='Foo Bar #3')
    sight_1 = TourSight.objects.create(title='Ocean')
    sight_2 = TourSight.objects.create(title='Lake')
    sight_3 = TourSight.objects.create(title='Mountains')
    tour.sights.add(sight_1)
    tour_2.sights.add(sight_2)
    tour_3.sights.add(sight_3)

    url = reverse('customer-tours-filtered-list')
    params = f'?sights__id={sight_1.id},{sight_2.id}'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    assert sorted([tour['title'] for tour in response.json()['tours']]) == sorted(['Foo Bar #1', 'Foo Bar #2'])


@pytest.mark.django_db
def test_should_return_tours_according_to_opportunities(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    tour = TourFactory(title='Foo Bar #1')
    tour_2 = TourFactory(title='Foo Bar #2')
    tour_3 = TourFactory(title='Foo Bar #3')
    opportunities_1 = TourOpportunity.objects.create(title='With dogs')
    opportunities_2 = TourOpportunity.objects.create(title='With bikes')
    opportunities_3 = TourOpportunity.objects.create(title='Without smartphones')
    tour.opportunities.add(opportunities_1)
    tour_2.opportunities.add(opportunities_2)
    tour_3.opportunities.add(opportunities_3)

    url = reverse('customer-tours-filtered-list')
    params = f'?opportunities__id={opportunities_1.id},{opportunities_3.id}'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    assert sorted([tour['title'] for tour in response.json()['tours']]) == sorted(['Foo Bar #1', 'Foo Bar #3'])


@pytest.mark.django_db
def test_should_return_tours_according_to_categories(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    tour_1 = TourFactory(title='Foo Bar #1')
    tour_2 = TourFactory(title='Foo Bar #2')
    tour_3 = TourFactory(title='Foo Bar #3')
    tour_4 = TourFactory(title='Foo Bar #1')

    category_1 = TourCategory.objects.create(title='hiking')
    category_2 = TourCategory.objects.create(title='running')
    category_3 = TourCategory.objects.create(title='weekend')
    category_4 = TourCategory.objects.create(title='trekking')
    tour_1.categories.add(category_1)
    tour_2.categories.add(category_2)
    tour_3.categories.add(category_2, category_3)
    tour_4.categories.add(category_4)

    url = reverse('customer-tours-filtered-list')
    params = '?categories__title=hiking,running'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 3
    actual_titles = [tour['title'] for tour in response.json()['tours']]
    assert sorted(actual_titles) == sorted(['Foo Bar #1', 'Foo Bar #2', 'Foo Bar #3'])


@pytest.mark.django_db
def test_should_return_tours_according_to_price(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    TourFactory(title='Foo Bar #1', price=500)
    TourFactory(title='Foo Bar #2', price=1000)
    TourFactory(title='Foo Bar #3', price=1450)
    TourFactory(title='Foo Bar #4', price=1510)

    url = reverse('customer-tours-filtered-list')
    params = '?price__gte=1000&price__lte=1500'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    assert sorted([tour['title'] for tour in response.json()['tours']]) == sorted(['Foo Bar #2', 'Foo Bar #3'])

