import pytest
from django.urls import reverse

from applications.treks.models import CustomerFavoriteTour
from tests.conftest import CustomerFactory, TourFactory, CustomerFavoriteTourFactory


@pytest.mark.django_db
def test_create_favorite_tour(api_client):
    tour = TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-favorite-tours')

    response = api_client.post(url, data={'tour': tour.id})

    assert response.status_code == 201
    fields = response.json().keys()
    assert CustomerFavoriteTour.objects.get(customer=user).tour == tour
    assert 'id' in fields
    assert 'title' in fields
    assert 'departure_at' in fields
    assert 'price' in fields
    assert 'tags' in fields
    assert 'image_urls' in fields
    assert 'currency' in fields
    assert 'description' in fields
    assert 'duration' in fields
    assert 'height' in fields
    assert 'distance' in fields


@pytest.mark.django_db
def test_retrieve_customer_favorite_tours(api_client):
    tour = TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-favorite-tours')

    response = api_client.post(url, data={'tour': tour.id})
    assert response.status_code == 201

    response = api_client.get(url)

    assert response.status_code == 200
    data = response.json()
    assert len(data['tours']) == 1
    assert data['tours'][0]['id'] == tour.id
    assert data['tours'][0]['title'] == tour.title
    assert data['tours'][0]['price'] == tour.price


@pytest.mark.django_db
def test_delete_customer_favorite_tour(api_client):
    tour = TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    response = api_client.post(reverse('customer-favorite-tours'), data={'tour': tour.id})
    assert CustomerFavoriteTour.objects.filter(customer=user).count() == 1

    favorite_tour_id = response.json()['id']
    response = api_client.put(reverse('customer-favorite-tour-destroy', args=(favorite_tour_id, )))

    assert response.status_code == 200
    assert CustomerFavoriteTour.objects.filter(customer=user).count() == 0
    fields = response.json().keys()
    assert 'id' in fields
    assert 'title' in fields
    assert 'departure_at' in fields
    assert 'price' in fields
    assert 'tags' in fields
    assert 'image_urls' in fields
    assert 'currency' in fields
    assert 'description' in fields
    assert 'duration' in fields
    assert 'height' in fields
    assert 'distance' in fields


@pytest.mark.django_db
def test_should_not_allow_to_add_tour_twice(api_client):
    user = CustomerFactory()
    tour = TourFactory()
    api_client.login(username=user.username, password='fooBar')
    CustomerFavoriteTourFactory(customer=user, tour=tour)

    response = api_client.post(reverse('customer-favorite-tours'), data={'tour': tour.id})

    assert response.status_code == 409
    assert response.json() == {'detail': 'The tour is already in your favorites.', 'status_code': 409}

