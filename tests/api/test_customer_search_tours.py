import pytest
from django.urls import reverse

from tests.conftest import CustomerFactory, TourFactory


@pytest.mark.django_db
def test_should_search_by_title(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    TourFactory(title='Belogorka')
    TourFactory(title='Racek')
    TourFactory(title='Abolish-Ata')

    url = reverse('customer-tours-search')
    params = '?search=Racek'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['title'] == 'Racek'

    url = reverse('customer-tours-search')
    params = '?search=ata'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['title'] == 'Abolish-Ata'


@pytest.mark.django_db
def test_should_search_by_owner_title(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    TourFactory(title='Ala-Archa', owner__title='Mount Club')
    TourFactory(title='Adage', owner__title='Racek')
    TourFactory(title='Kol-Tor', owner__title='First Club')

    url = reverse('customer-tours-search')
    params = '?search=club'
    response = api_client.get(url + params)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    assert 'Ala-Archa' in [tour['title'] for tour in response.json()['tours']]
    assert 'Kol-Tor' in [tour['title'] for tour in response.json()['tours']]
