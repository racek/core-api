import pytest
from django.urls import reverse

from tests.conftest import GuideCompanyFactory, OrderFeedbackFactory


@pytest.mark.django_db
def test_return_company_profile(api_client):
    company = GuideCompanyFactory(title='Foo', email='bar@foo.com', description='some foobar description')

    response = api_client.get(reverse('company-profile', args=(company.id, )))

    assert response.status_code == 200
    assert response.json()['id'] == company.id
    assert response.json()['title'] == 'Foo'
    assert response.json()['icon_url'] == company.icon.url
    assert response.json()['email'] == 'bar@foo.com'
    assert response.json()['description'] == 'some foobar description'


@pytest.mark.django_db
def test_return_company_rating(api_client):
    company = GuideCompanyFactory()
    OrderFeedbackFactory(tour_company=company, tour_score=2)
    OrderFeedbackFactory(tour_company=company, tour_score=3)
    OrderFeedbackFactory(tour_company=company, tour_score=5)

    response = api_client.get(reverse('company-profile', args=(company.id, )))

    assert response.status_code == 200
    assert response.json()['rating'] == 3.33
