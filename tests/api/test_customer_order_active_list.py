from datetime import datetime

import pytest
from django.urls import reverse
from freezegun import freeze_time

from applications.orders.models import Order
from tests.conftest import OrderFactory, CustomerFactory, TourFactory


@pytest.mark.django_db
def test_return_customer_ordered_tours(api_client):
    tour = TourFactory(description='Foo Bar', duration=300, distance=550, height=3500)
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    order = OrderFactory(buyer=user, tour=tour)

    response = api_client.get(reverse('customer-active-orders-list'))

    assert response.status_code == 200
    assert len(response.json()['orders']) == 1
    assert response.json()['orders'][0]['title'] == tour.title
    assert response.json()['orders'][0]['id'] == order.id
    assert response.json()['orders'][0]['description'] == 'Foo Bar'
    assert response.json()['orders'][0]['duration'] == 300
    assert response.json()['orders'][0]['distance'] == 550
    assert response.json()['orders'][0]['height'] == 3500
    assert '.jpg' in response.json()['orders'][0]['owner_icon_url']


@pytest.mark.django_db
def test_return_orders_in_active_status(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    OrderFactory(buyer=customer, status=Order.USED)

    response = api_client.get(reverse('customer-active-orders-list'))

    assert response.status_code == 200
    assert len(response.json()['orders']) == 0

    OrderFactory(buyer=customer, status=Order.IDLE)
    OrderFactory(buyer=customer, status=Order.CONFIRMED)

    response = api_client.get(reverse('customer-active-orders-list'))
    assert response.status_code == 200
    assert len(response.json()['orders']) == 2


@pytest.mark.django_db
def test_return_orders_are_not_yet_completed(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')

    OrderFactory(buyer=customer, status=Order.IDLE, tour__arrival_at=datetime(2021, 2, 16, 15, 30))

    with freeze_time('2021-02-16 15:29'):
        response = api_client.get(reverse('customer-active-orders-list'))

        assert response.status_code == 200
        assert len(response.json()['orders']) == 1

    with freeze_time('2021-02-16 15:31'):
        response = api_client.get(reverse('customer-active-orders-list'))

        assert response.status_code == 200
        assert len(response.json()['orders']) == 0
