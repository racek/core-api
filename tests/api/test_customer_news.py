import pytest
from django.urls import reverse

from applications.news.models import CustomerNewsViewLog
from tests.conftest import CustomerNewsFactory, CustomerFactory, CustomerNewsViewLogFactory


@pytest.mark.django_db
def test_return_public_news(api_client):
    news = CustomerNewsFactory(public=True)
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')

    response = api_client.get(reverse('customer-news-list'))
    assert response.status_code == 200
    assert len(response.json()['news']) == 1

    news.public = False
    news.save()

    response = api_client.get(reverse('customer-news-list'))
    assert response.status_code == 200
    assert len(response.json()['news']) == 0


@pytest.mark.django_db
def test_return_private_news_correctly(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    news = CustomerNewsFactory(public=False)
    news.customers.add(customer)

    response = api_client.get(reverse('customer-news-list'))
    assert response.status_code == 200
    assert len(response.json()['news']) == 1

    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')

    response = api_client.get(reverse('customer-news-list'))
    assert response.status_code == 200
    assert len(response.json()['news']) == 0


@pytest.mark.django_db
def test_should_know_when_news_viewed(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    news = CustomerNewsFactory(public=True)

    response = api_client.get(reverse('customer-news-list'))
    assert response.status_code == 200
    assert response.json()['news'][0]['viewed'] == False

    CustomerNewsViewLogFactory(customer=customer, news=news)

    response = api_client.get(reverse('customer-news-list'))
    assert response.status_code == 200
    assert response.json()['news'][0]['viewed'] == True


@pytest.mark.django_db
def test_contain_determined_fields(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    CustomerNewsFactory(public=True)

    response = api_client.get(reverse('customer-news-list'))

    assert response.status_code == 200
    fields = response.json()['news'][0].keys()
    assert 'title' in fields
    assert 'body' in fields
    assert 'viewed' in fields


@pytest.mark.django_db
def test_return_news_detail(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    news = CustomerNewsFactory(public=True)

    response = api_client.get(reverse('customer-news-detail', args=(news.id,)))

    assert response.status_code == 200
    assert response.json()['title'] == news.title
    assert response.json()['body'] == news.body
    assert response.json()['viewed'] == False


@pytest.mark.django_db
def test_mark_news_as_viewed(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    news = CustomerNewsFactory(public=True)

    response = api_client.patch(reverse('customer-news-detail', args=(news.id,)))

    assert response.status_code == 201
    assert CustomerNewsViewLog.objects.get(customer=customer).news == news


@pytest.mark.django_db
def test_return_news_with_pagination(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    CustomerNewsFactory.create_batch(125, public=True)

    response = api_client.get(reverse('customer-news-list'))

    assert response.status_code == 200
    assert response.json()['total_size'] == 125
    assert len(response.json()['news']) == 25
