import json
import pytest
from django.urls import reverse

from tests.conftest import OrderFactory, CustomerFactory, TourFactory


@pytest.mark.django_db
def test_should_return_top_tours_by_orders(api_client):
    user = CustomerFactory
    tour1 = TourFactory()
    tour2 = TourFactory()
    tour3 = TourFactory()
    OrderFactory(tour=tour1, seats=1)
    OrderFactory(tour=tour3, seats=2)
    OrderFactory(tour=tour3, seats=3)
    OrderFactory(tour=tour2, seats=2)
    OrderFactory(tour=tour2, seats=1)
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-top-tours')

    response = api_client.get(url)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 3
    assert response.json()['tours'][0]['id'] == tour3.id
    assert response.json()['tours'][1]['id'] == tour2.id
    assert response.json()['tours'][2]['id'] == tour1.id
