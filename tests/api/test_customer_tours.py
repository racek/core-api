from collections import Counter
from datetime import datetime

import pytest
import pytz
from django.urls import reverse
from django.utils import timezone

from applications.orders.models import Order
from applications.treks.models import TourCategory
from tests.conftest import (
    CustomerFactory,
    OrderFactory,
    TourFactory,
    TourSightFactory,
    TourOpportunityFactory,
    GuideCompanyFactory,
)


@pytest.mark.django_db
def test_should_return_empty_array_when_tours_does_not_exist(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-list')

    response = api_client.get(url)

    assert response.status_code == 200
    assert response.json() == {'tours': []}


@pytest.mark.django_db
def test_should_return_tours(api_client):
    tour = TourFactory(departure_at=timezone.now() + timezone.timedelta(days=1))
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-list')

    response = api_client.get(url)

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['title'] == tour.title


@pytest.mark.django_db
def test_should_return_determined_fields(api_client):
    TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-list')

    response = api_client.get(url)

    assert response.status_code == 200
    fields = response.json()['tours'][0].keys()
    assert 'id' in fields
    assert 'title' in fields
    assert 'departure_at' in fields
    assert 'price' in fields
    assert 'tags' in fields
    assert 'image_urls' in fields
    assert 'currency' in fields


@pytest.mark.django_db
def test_should_return_active_order_id(api_client):
    user = CustomerFactory()
    OrderFactory(buyer=user, status=Order.CANCELED)
    order = OrderFactory(buyer=user, status=Order.IDLE)

    api_client.login(username=user.username, password='fooBar')
    response = api_client.get(reverse('customer-tour-list'))

    assert response.status_code == 200
    assert len(response.json()['tours']) == 2
    actual_order_ids = [tour['order_id'] for tour in response.json()['tours']]
    assert Counter(actual_order_ids) == Counter([None, order.id])


@pytest.mark.django_db
def test_should_not_return_order_id_when_customer_has_not_purchased_the_tour(api_client):
    user = CustomerFactory()
    OrderFactory()

    api_client.login(username=user.username, password='fooBar')
    response = api_client.get(reverse('customer-tour-list'))

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['order_id'] is None


@pytest.mark.django_db
def test_return_not_found_when_tour_does_not_exist(api_client):
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-detail', args=(1,))

    response = api_client.get(url)

    assert response.status_code == 404
    assert response.json() == {'detail': 'Страница не найдена.', 'status_code': 404}


@pytest.mark.django_db
def test_return_tour_details(api_client):
    tour = TourFactory(available_tickets_count=4)
    category_1 = TourCategory.objects.create(title='foo_bar')
    category_2 = TourCategory.objects.create(title='bar_foo')
    tour.categories.add(category_1, category_2)

    OrderFactory(tour=tour, seats=2)
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-detail', args=(tour.id,))

    response = api_client.get(url)

    assert response.status_code == 200
    fields = response.json().keys()
    assert 'id' in fields
    assert 'title' in fields
    assert 'departure_at' in fields
    assert 'price' in fields
    assert 'tags' in fields
    assert 'image_urls' in fields
    assert 'currency' in fields
    assert 'description' in fields
    assert 'duration' in fields
    assert 'height' in fields
    assert 'distance' in fields
    assert 2, response.json()['available_for_purchase_count']
    assert ['foo_bar', 'bar_foo'], response.json()['categories']


@pytest.mark.django_db
def test_return_tours_given_category(api_client):
    tour_1 = TourFactory()
    tour_2 = TourFactory()
    category_1 = TourCategory.objects.create(title='walking')
    category_2 = TourCategory.objects.create(title='running')
    tour_1.categories.add(category_1)
    tour_2.categories.add(category_2)

    url = reverse('customer-tour-list')

    response = api_client.get(url + '?category=running')

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['id'] == tour_2.id


@pytest.mark.django_db
def test_should_return_existing_sights(api_client):
    sight = TourSightFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-sights-list')

    response = api_client.get(url)

    assert response.status_code == 200
    assert len(response.json()['sights']) == 1
    assert response.json()['sights'][0]['id'] == sight.id
    assert response.json()['sights'][0]['title'] == sight.title


@pytest.mark.django_db
def test_should_return_existing_tour_opportunities(api_client):
    opportunity = TourOpportunityFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-tour-opportunities-list')

    response = api_client.get(url)

    assert response.status_code == 200
    assert len(response.json()['opportunities']) == 1
    assert response.json()['opportunities'][0]['id'] == opportunity.id
    assert response.json()['opportunities'][0]['title'] == opportunity.title


@pytest.mark.django_db
def test_return_company_tours(api_client):
    company = GuideCompanyFactory()
    TourFactory.create_batch(10)
    TourFactory(owner=company, title='Racek Bar')

    url = reverse('customer-tour-list')
    response = api_client.get(url + f'?company_id={company.id}')

    assert response.status_code == 200
    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['title'] == 'Racek Bar'
