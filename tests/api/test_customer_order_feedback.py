import pytest
from django.urls import reverse

from applications.orders.models import OrderFeedback
from tests.conftest import OrderFactory, CustomerFactory, TourFactory, OrderFeedbackFactory


@pytest.mark.django_db
def test_customer_can_create_feedback(api_client):
    order = OrderFactory()
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-order-feedback')

    payload = {'order': order.id, 'tour_score': 2, 'guide_score': 1, 'message': 'test message'}
    response = api_client.post(url, data=payload)

    assert response.status_code == 201
    feedback = OrderFeedback.objects.get(order=order)
    assert feedback.tour_company == order.tour.owner
    assert feedback.tour_score == 2
    assert feedback.guide_score == 1
    assert feedback.message == 'test message'


@pytest.mark.django_db
def test_raise_error_when_tour_does_not_have_guide(api_client):
    tour = TourFactory(guide=None)
    order = OrderFactory(tour=tour)
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-order-feedback')

    payload = {'order': order.id, 'tour_score': 2, 'guide_score': 1, 'message': 'test message'}
    response = api_client.post(url, data=payload)

    assert response.status_code == 400
    assert response.json()['detail'] == 'The tour does not have a guide.'


@pytest.mark.django_db
def test_raise_error_when_feedback_already_exists(api_client):
    order = OrderFactory()
    OrderFeedbackFactory(order=order)
    customer = CustomerFactory()
    payload = {'order': order.id, 'tour_score': 2, 'guide_score': 1, 'message': 'test message'}

    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-order-feedback')
    response = api_client.post(url, data=payload)

    assert response.status_code == 409
    assert response.json()['detail'] == 'The feedback already exists.'

