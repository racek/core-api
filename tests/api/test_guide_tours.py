import pytest
from django.urls import reverse
from rest_framework import status

from applications.orders.models import Order
from applications.treks.models import Tour
from tests.conftest import TourFactory, GuideFactory, OrderFactory


@pytest.mark.django_db
def test_should_return_only_guides_tours(api_client):
    guide = GuideFactory()
    tour = TourFactory(guide=guide)
    TourFactory.create_batch(10)

    api_client.login(username=guide.username, password='fooBar')
    response = api_client.get(reverse('guide-tour-list'))

    assert len(response.json()['tours']) == 1
    assert response.json()['tours'][0]['id'] == tour.id


@pytest.mark.django_db
def test_should_return_tour_orders(api_client):
    guide = GuideFactory()
    tour = TourFactory()
    OrderFactory.create_batch(10, tour=tour, status=Order.USED)
    OrderFactory.create_batch(10, tour=tour, status=Order.CANCELED)
    OrderFactory.create_batch(10, status=Order.IDLE)
    order_1 = OrderFactory(tour=tour, status=Order.IDLE)
    order_2 = OrderFactory(tour=tour, status=Order.CONFIRMED)

    api_client.login(username=guide.username, password='fooBar')
    response = api_client.get(reverse('guide-tour-order-list', args=(tour.id, )))

    assert len(response.json()['orders']) == 2
    assert sorted([order['id'] for order in response.json()['orders']]) == sorted([order_1.id, order_2.id])


@pytest.mark.django_db
def test_should_not_accept_put_method(api_client):
    tour = TourFactory()
    guide = GuideFactory()
    url = reverse('guide-tour-detail', args=(tour.id, ))

    api_client.login(username=guide.username, password='fooBar')
    response = api_client.put(url)

    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db
def test_should_mark_tour_as_finished(api_client):
    guide = GuideFactory()
    tour = TourFactory()
    OrderFactory.create_batch(10, tour=tour, status=Order.CONFIRMED)
    OrderFactory.create_batch(10, tour=tour, status=Order.IDLE)

    api_client.login(username=guide.username, password='fooBar')
    url = reverse('guide-tour-detail', args=(tour.id, ))
    response = api_client.patch(url, data={'status': Tour.FINISHED})

    tour.refresh_from_db()
    assert response.status_code == 200
    assert tour.status == Tour.FINISHED
    assert tour.actual_finished_at is not None
    assert tour.order_set.count() == 20
    assert tour.order_set.filter(status=Order.USED).count() == 10
    assert tour.order_set.filter(status=Order.MISSED).count() == 10
