from datetime import datetime

import pytest
from django.urls import reverse
from freezegun import freeze_time

from applications.orders.models import Order
from tests.conftest import CustomerFactory, OrderFactory


@pytest.mark.django_db
def test_should_return_orders_which_departure_at_expired(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-orders-history-list')
    OrderFactory(buyer=customer, tour__departure_at=datetime(2021, 2, 16, 15, 30), status=Order.IDLE)

    with freeze_time('2021-02-16 15:29'):
        response = api_client.get(url)

        assert response.status_code == 200
        assert len(response.json()['orders']) == 0

    with freeze_time('2021-02-16 15:31'):
        response = api_client.get(url)

        assert response.status_code == 200
        assert len(response.json()['orders']) == 1


@pytest.mark.django_db
def test_return_orders_which_not_in_active_status(api_client):
    customer = CustomerFactory()
    api_client.login(username=customer.username, password='fooBar')
    url = reverse('customer-orders-history-list')

    OrderFactory(buyer=customer, status=Order.IDLE)

    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.json()['orders']) == 0

    OrderFactory(buyer=customer, status=Order.USED)

    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.json()['orders']) == 1
