import pytest
from django.urls import reverse

from tests.conftest import GuideFactory, OrderFeedbackFactory
from applications.users.models import User


@pytest.mark.django_db
def test_guide_can_get_own_profile(api_client):
    GuideFactory()
    GuideFactory()
    guide = GuideFactory(first_name='Foo', last_name='Bar')

    api_client.login(username=guide.username, password='fooBar')
    response = api_client.get(reverse('guide-profile'))

    assert response.status_code == 200
    assert response.json()['id'] == guide.id
    assert response.json()['fullname'] == 'Foo Bar'


@pytest.mark.django_db
def test_guide_can_get_own_profile1(api_client):
    guide = GuideFactory(city=User.OSH)
    OrderFeedbackFactory(guide=guide, guide_score=3)
    OrderFeedbackFactory(guide=guide, guide_score=5)

    api_client.login(username=guide.username, password='fooBar')
    response = api_client.get(reverse('guide-profile'))

    assert response.status_code == 200
    assert response.json()['id'] == guide.id
    assert response.json()['rating'] == 4.0
    assert response.json()['city'] == User.OSH

