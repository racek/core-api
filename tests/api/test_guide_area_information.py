import pytest
from django.urls import reverse

from tests.conftest import GuideFactory
from applications.core.models import AreaInformation


@pytest.mark.django_db
def test_should_return_area_information(api_client):
    guide = GuideFactory()
    AreaInformation.objects.create(title='Racek', text='Foo description')

    api_client.login(username=guide.username, password='fooBar')
    response = api_client.get(reverse('guide-area-information-list'))

    assert response.status_code == 200
    assert len(response.json()['information']) == 1
    assert response.json()['information'][0]['title'] == 'Racek'
    assert response.json()['information'][0]['description'] == 'Foo description'
