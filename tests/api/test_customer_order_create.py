import json
from datetime import datetime

import pytest
from django.urls import reverse
from freezegun import freeze_time
from rest_framework import status

from applications.orders.models import Order, AdditionalPassenger
from tests.conftest import CustomerFactory, OrderFactory, TourFactory


@pytest.mark.django_db
def test_customer_booking_tour(api_client):
    tour = TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-orders-create')

    payload = {
        'tour': tour.id,
        'seats': 1,
    }
    response = api_client.post(url, data=json.dumps(payload), content_type='application/json')

    assert response.status_code == 201
    assert response.json()['owner_id'] == tour.owner_id
    assert Order.objects.get(tour=tour).buyer == user


@pytest.mark.django_db
def test_customer_can_booking_with_additional_passenger(api_client):
    tour = TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-orders-create')

    payload = {
        'tour': tour.id,
        'seats': 2,
        'additional_passengers': [
            {
                'fullname': 'Foo Bar',
                'phone_number': '+996555777888',
            }
        ],
    }
    response = api_client.post(url, data=json.dumps(payload), content_type='application/json')

    assert response.status_code == 201
    assert Order.objects.get(tour=tour).buyer == user
    assert AdditionalPassenger.objects.get(order__tour=tour).phone_number == '+996555777888'


@pytest.mark.django_db
def test_raise_error_when_customer_already_booked_tour(api_client):
    tour = TourFactory()
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    OrderFactory(buyer=user, tour=tour)

    url = reverse('customer-orders-create')
    response = api_client.post(url, data={'tour': tour.id, 'seats': 1})

    assert response.status_code == status.HTTP_409_CONFLICT
    assert response.json() == {'detail': 'Tour already booked.', 'status_code': 409}


@pytest.mark.django_db
def test_raise_error_when_tour_departure_at_has_expired(api_client):
    tour = TourFactory(departure_at=datetime(2021, 2, 16, 13, 30))
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-orders-create')

    with freeze_time('2021-02-16 13:30:01'):
        response = api_client.post(url, data={'tour': tour.id, 'seats': 1})

    assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE


@pytest.mark.django_db
def test_raise_error_when_tour_has_not_available_seats(api_client):
    tour = TourFactory(available_tickets_count=3)
    OrderFactory(tour=tour, seats=2)
    user = CustomerFactory()
    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-orders-create')

    response = api_client.post(url, data={'tour': tour.id, 'seats': 2})

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {'detail': 'No available tickets.', 'status_code': 400}


@pytest.mark.django_db
def test_should_generate_correct_order_code(api_client):
    tour = TourFactory()
    OrderFactory(tour=tour, code=150)
    user = CustomerFactory()

    api_client.login(username=user.username, password='fooBar')
    url = reverse('customer-orders-create')
    response = api_client.post(url, data={'tour': tour.id, 'seats': 1})

    assert response.status_code == status.HTTP_201_CREATED
    assert not Order.objects.get(tour=tour, buyer=user).code == 150
