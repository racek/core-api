import uuid
from random import randint

import factory
import pytest
from django.conf import settings
from django.utils import timezone
from pytest_factoryboy import register
from rest_framework.test import APIClient

from applications.news.models import CustomerNews, CustomerNewsViewLog
from applications.orders.models import Order, OrderFeedback
from applications.treks.models import Tour, CustomerFavoriteTour, TourSight, TourOpportunity
from applications.users.models import TourGroup


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def staff(db, django_user_model):
    def make_user(**kwargs):
        kwargs['password'] = 'fooBar'
        if 'username' not in kwargs:
            kwargs['username'] = str(uuid.uuid4())
        return django_user_model.objects.create_user(**kwargs)

    return make_user


@register
class CustomerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = settings.AUTH_USER_MODEL
        django_get_or_create = ('username',)

    username = factory.faker.Faker('word')
    password = factory.PostGenerationMethodCall('set_password', 'fooBar')
    is_customer = True


@register
class GuideFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = settings.AUTH_USER_MODEL
        django_get_or_create = ('username',)

    username = factory.faker.Faker('word')
    password = factory.PostGenerationMethodCall('set_password', 'fooBar')
    is_guide = True
    guide_avatar = factory.django.ImageField()


@register
class GuideCompanyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TourGroup

    title = 'In Tourist'
    icon = factory.django.ImageField()


@register
class TourFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tour

    title = 'Ala-Arch Resort'
    owner = factory.SubFactory(GuideCompanyFactory)
    guide = factory.SubFactory(GuideFactory)
    description = 'Some description'
    price = 1500
    duration = 1
    available_tickets_count = 2

    @factory.lazy_attribute
    def departure_at(self):
        return timezone.now() + timezone.timedelta(days=1)


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    buyer = factory.SubFactory(CustomerFactory)
    tour = factory.SubFactory(TourFactory)
    price = 1200


class CustomerNewsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CustomerNews

    title = 'Foo Bar Title'
    body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac lacinia arcu. Sed ac ex.'


class CustomerNewsViewLogFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CustomerNewsViewLog

    customer = factory.SubFactory(CustomerFactory)
    news = factory.SubFactory(CustomerNewsFactory)


class CustomerFavoriteTourFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CustomerFavoriteTour

    customer = factory.SubFactory(CustomerFactory)
    tour = factory.SubFactory(TourFactory)


class TourSightFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TourSight

    title = factory.faker.Faker('word')


class TourOpportunityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TourOpportunity

    title = factory.faker.Faker('word')


class OrderFeedbackFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OrderFeedback

    order = factory.SubFactory(OrderFactory)
    customer = factory.SubFactory(CustomerFactory)
    guide = factory.SubFactory(GuideFactory)
    tour_company = factory.SubFactory(GuideCompanyFactory)
    tour_score = randint(1, 5)
    guide_score = randint(1, 5)
    message = factory.faker.Faker('word')
